#include "stdafx.h"
#include "MyEditor.h"
#include "KeyMgr.h"
#include "LineMgr.h"
#include "ScrollMgr.h"


CMyEditor::CMyEditor()
{
}


CMyEditor::~CMyEditor()
{
	Release();
}

void CMyEditor::Initialize()
{
	m_DC = GetDC(g_hWnd);
}

void CMyEditor::Update()
{
	if (CKeyMgr::Get_Instance()->Key_Pressing(VK_LEFT))
		CScrollMgr::Get_Instance()->Set_ScrollX(5.f);
	if (CKeyMgr::Get_Instance()->Key_Pressing(VK_RIGHT))
		CScrollMgr::Get_Instance()->Set_ScrollX(-5.f);

	if (CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON))
		CLineMgr::Get_Instance()->Add_Line();

	if (CKeyMgr::Get_Instance()->Key_Down('A'))
		CLineMgr::Get_Instance()->Save_Line();
	if (CKeyMgr::Get_Instance()->Key_Down('S'))
		CLineMgr::Get_Instance()->Load_Line();
}

void CMyEditor::Late_Update()
{
	CKeyMgr::Get_Instance()->Key_Update();
}

void CMyEditor::Render()
{
	Rectangle(m_DC, 0, 0, WINCX, WINCY);

	CLineMgr::Get_Instance()->Render(m_DC);
}

void CMyEditor::Release()
{
	CKeyMgr::Destroy_Instance();
	CScrollMgr::Destroy_Instance();
	CLineMgr::Destroy_Instance();

	ReleaseDC(g_hWnd, m_DC);
}
