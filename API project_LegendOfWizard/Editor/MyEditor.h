#pragma once

#ifndef __MYEDITOR_H__
#define __MYEDITOR_H__


class CMyEditor
{
public:
	CMyEditor();
	~CMyEditor();

public:
	void Initialize();
	void Update();
	void Late_Update();
	void Render();
	void Release();

private:
	HDC		m_DC;
};

#endif // !__MYEDITOR_H__


