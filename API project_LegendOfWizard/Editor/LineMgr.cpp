#include "stdafx.h"
#include "LineMgr.h"
#include "Line.h"
#include "ScrollMgr.h"

CLineMgr* CLineMgr::m_pInstance = nullptr;
CLineMgr::CLineMgr()
{
	ZeroMemory(m_tPos, sizeof(m_tPos));
}


CLineMgr::~CLineMgr()
{
	Release();
}

void CLineMgr::Initialize()
{
	//LINEPOS		tLinePos[4] = { {100.f, 500.f}, {300.f, 500.f}, {500.f, 300.f}, {700.f, 300.f} };
	//m_listLine.emplace_back(new CLine(tLinePos[0], tLinePos[1]));
	//m_listLine.emplace_back(new CLine(tLinePos[1], tLinePos[2]));
	//m_listLine.emplace_back(new CLine(tLinePos[2], tLinePos[3]));
}

void CLineMgr::Render(HDC _DC)
{
	for (auto& pLine : m_listLine)
		pLine->Render(_DC);
}

void CLineMgr::Release()
{
	for_each(m_listLine.begin(), m_listLine.end(), Safe_Delete<CLine*>);
	m_listLine.clear();
}

void CLineMgr::Add_Line()
{
	POINT	pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);

	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();

	pt.x -= iScrollX;

	if (!m_tPos[0].fX && !m_tPos[0].fY)
	{
		m_tPos[0].fX = (float)pt.x;
		m_tPos[0].fY = (float)pt.y;
	}
	else
	{
		m_tPos[1].fX = (float)pt.x;
		m_tPos[1].fY = (float)pt.y;

		m_listLine.emplace_back(new CLine(m_tPos[0], m_tPos[1]));

		//ZeroMemory(&m_tPos[0], sizeof(m_tPos[0]));
		memcpy(&m_tPos[0], &m_tPos[1], sizeof(m_tPos[0]));
	}
}

void CLineMgr::Save_Line()
{
	HANDLE hFile = CreateFile(L"../Data/Line.dat", GENERIC_WRITE
		, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"저장 실패!", L"실패", MB_OK);
		return;
	}

	DWORD	dwByte = 0;

	for (auto& pLine : m_listLine)
		WriteFile(hFile, &pLine->Get_Info(), sizeof(LINEINFO), &dwByte, NULL);

	MessageBox(g_hWnd, L"저장 성공!", L"성공", MB_OK);
	CloseHandle(hFile);
}

void CLineMgr::Load_Line()
{
	HANDLE hFile = CreateFile(L"../Data/Line.dat", GENERIC_READ
		, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"불러오기 실패!", L"실패", MB_OK);
		return;
	}

	DWORD	dwByte = 0;
	LINEINFO Temp = {};

	while (true)
	{
		ReadFile(hFile, &Temp, sizeof(LINEINFO), &dwByte, NULL);

		if (0 == dwByte)
			break;

		m_listLine.emplace_back(new CLine(Temp.tLeftPos, Temp.tRightPos));
	}

	MessageBox(g_hWnd, L"불러오기 성공!", L"성공", MB_OK);
	CloseHandle(hFile);
}

bool CLineMgr::Collision_Line(float _x, float* _y)
{
	if (m_listLine.empty())
		return false;

	CLine* pTarget = nullptr;

	for (auto& pLine : m_listLine)
	{
		if (pLine->Get_Info().tLeftPos.fX <= _x &&
			pLine->Get_Info().tRightPos.fX >= _x)
		{
			float x1 = pLine->Get_Info().tLeftPos.fX;
			float y1 = pLine->Get_Info().tLeftPos.fY;
			float x2 = pLine->Get_Info().tRightPos.fX;
			float y2 = pLine->Get_Info().tRightPos.fY;

			*_y = ((y2 - y1) / (x2 - x1)) * (_x - x1) + y1;
			return true;
		}
	}

	return false;
}
