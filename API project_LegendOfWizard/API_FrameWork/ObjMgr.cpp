#include "stdafx.h"
#include "ObjMgr.h"
#include "Obj.h"
#include "CollisionMgr.h"
#include "KeyMgr.h"

CObjMgr* CObjMgr::m_pInstance = nullptr;

CObjMgr::CObjMgr()
{
	isHitBoxRender = false;
	m_IsImmortal = false;
}


CObjMgr::~CObjMgr()
{
	Release();
}

void CObjMgr::Add_Object(OBJID::ID _eID, CObj* _pObj)
{
	m_listObj[_eID].emplace_back(_pObj);
}

void CObjMgr::Update()
{
	KeyCheck();
	
	if (CKeyMgr::Get_Instance()->Key_Down('O')) {
		isHitBoxRender = !isHitBoxRender;
	}

	for (int i = 0; i < OBJID::END; ++i)
	{
		auto& iter = m_listObj[i].begin();
		for (; iter != m_listObj[i].end(); )
		{
			int iEvent = (*iter)->Update();
			if (OBJ_DEAD == iEvent)
			{
				SAFE_DELETE(*iter);
				iter = m_listObj[i].erase(iter);
			}
			else
				++iter;
		}
	}

	//CCollisionMgr::Collision_Rect(m_listObj[OBJID::BULLET], m_listObj[OBJID::MONSTER]);
	//CCollisionMgr::Collision_Rect_Ex(m_listObj[OBJID::PLAYER], m_listObj[OBJID::MONSTER]);
	//CCollisionMgr::Collision_Rect_Ex(m_listObj[OBJID::MONSTER], m_listObj[OBJID::PLAYER]);

	//Player Bullet과 Monster들의 충돌
	CCollisionMgr::HitBoxCollision(m_listObj[OBJID::BULLET], m_listObj[OBJID::MONSTER]);
	CCollisionMgr::HitBoxCollision(m_listObj[OBJID::BULLET], m_listObj[OBJID::MIDDLEBOSS]);

	//Player Shield와 Monster 공격들의 충돌
	//CCollisionMgr::HitBoxCollision(m_listObj[OBJID::SHIELD], m_listObj[OBJID::MONSTERBULLET]);
	CCollisionMgr::Collision_RectHit( m_listObj[OBJID::MONSTERBULLET], m_listObj[OBJID::SHIELD]);

	//Player와 Monster들의 공격들의 충돌
	if (m_IsImmortal == false) {
		CCollisionMgr::HitBoxCollision(m_listObj[OBJID::PLAYER], m_listObj[OBJID::MONSTERBULLET]);
	}

	//Player와 Coin의 충돌
	CCollisionMgr::Collision_RectCoin(m_listObj[OBJID::COIN], m_listObj[OBJID::PLAYER]);

}

void CObjMgr::Late_Update()
{
	for (int i = 0; i < OBJID::END; ++i)
	{
		for (auto& pObj : m_listObj[i])
		{
			pObj->Late_Update();
			if (m_listObj[i].empty())
				break;

			RENDERID::ID eID = pObj->Get_RenderID();
			m_listRender[eID].emplace_back(pObj);
		}
	}
}

void CObjMgr::Render(HDC _DC)
{
	for (int i = 0; i < RENDERID::END; ++i) {
		if (RENDERID::OBJECT == i)
			m_listRender[i].sort(CompareY<CObj*>);

		for (auto& pObj : m_listRender[i]) {
			pObj->Render(_DC);

			if (isHitBoxRender) {
				pObj->RenderHitBox(_DC);
			}
		}	

		m_listRender[i].clear();
	}

	
}

void CObjMgr::Release()
{
	for (int i = 0; i < OBJID::END; ++i)
	{
		for_each(m_listObj[i].begin(), m_listObj[i].end(), Safe_Delete<CObj*>);
		m_listObj[i].clear();
	}
}

CObj* CObjMgr::Get_Target(CObj* _pObj, OBJID::ID _eID)
{
	if (m_listObj[_eID].empty())
		return nullptr;

	CObj*	pReal = nullptr;
	float	fDis = 0.f;

	for (auto& pTarget : m_listObj[_eID])
	{
		float	fX = pTarget->Get_INFO().fX - _pObj->Get_INFO().fX;
		float	fY = pTarget->Get_INFO().fY - _pObj->Get_INFO().fY;
		float	fDia = sqrtf(fX * fX + fY * fY);

		if (fDis > fDia || !pReal)
		{
			pReal = pTarget;
			fDis = fDia;
		}
	}

	return pReal;
}

void CObjMgr::Delete_ID(OBJID::ID _eID)
{
	for_each(m_listObj[_eID].begin(), m_listObj[_eID].end(), Safe_Delete<CObj*>);
	m_listObj[_eID].clear();
}


void CObjMgr::KeyCheck() {
	if (CKeyMgr::Get_Instance()->Key_Down('P')) {
	m_IsImmortal = !m_IsImmortal;
	}
}