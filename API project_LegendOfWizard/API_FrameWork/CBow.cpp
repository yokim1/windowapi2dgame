#include "stdafx.h"
#include "CBow.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CArcherAttack.h"
#include "SoundMgr.h"

CBow::CBow()
{
}


CBow::~CBow()
{
}


void CBow::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ARCHER_BOW_LEFT.bmp", L"BOW_LEFT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ARCHER_BOW_RIGHT.bmp", L"BOW_RIGHT");

	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = 1.f;

	m_eRenderID = RENDERID::EFFECT;

	m_pFrameKey = L"BOW_RIGHT";

	m_ePreState = END;
}

int CBow::Update() {
	if (m_bDead)
		return OBJ_DEAD;

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };

	float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
	float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
	float fDia = sqrtf(fX * fX + fY * fY);

	float fRad = acosf(fX / fDia);

	//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
	//	fRad = 2 * PI - fRad;
	//m_fAngle = fRad * 180.f / PI;

	m_fAngle = fRad * 180.f / PI;
	if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		m_fAngle *= -1.f;

	/*m_tInfo.fX = cosf(m_fAngle * PI / 180.f);
	m_tInfo.fY = sinf(m_fAngle * PI / 180.f);*/


	if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
		m_pFrameKey = L"BOW_RIGHT";
	else
		m_pFrameKey = L"BOW_LEFT";


	Set_State();

	Update_Rect();
	Update_Range();
	Update_Direction();

	Update_Frame();

	Key_State();

	return OBJ_NOEVENT;
}

void CBow::Late_Update() {

}

void CBow::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 180, m_tFrame.iStateY * 170
		, 180, 170
		, RGB(0, 255, 255));

	//Rectangle(_DC, m_tRect.left, m_tRect.top, m_tRect.right, m_tRect.bottom);
}

void CBow::Release() {

}


void CBow::Update_Frame() {
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX == 3) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CArcherAttack>());

			CSoundMgr::Get_Instance()->PlaySound(L"ARCHER_SHOOT.mp3", CSoundMgr::MONSTER);
		}

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			m_tFrame.iStartX = 0;
			
		}
	}
}

//I gotta use this for CArcherAttack to fix the direection problem

void CBow::Set_State() {
	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState)
		{
		case CBow::ATTACK: 
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.dwSpeed = 500;
			m_tFrame.dwTime = GetTickCount();
			break;
		}
		m_ePreState = m_eCurState;
	}
}

void CBow::Update_Range() {

}

void CBow::Update_Direction() {

	if ((0 <= m_fAngle && m_fAngle <= 11.25)) {
		m_tFrame.iStateY = RIGHT;
	}
	else if (11.25 <= m_fAngle && m_fAngle <= 33.75) {
		m_tFrame.iStateY = RIGHTUP225;
	}
	else if (33.75 <= m_fAngle && m_fAngle <= 56.25) {
		m_tFrame.iStateY = RIGHTUP45;
	}
	else if (56.25 <= m_fAngle && m_fAngle <= 78.75) {
		m_tFrame.iStateY = RIGHTUP675;
	}
	else if (78.75 <= m_fAngle && m_fAngle <= 101.25) {
		m_tFrame.iStateY = RIGHTUP;
	}
	else if (101.25 <= m_fAngle && m_fAngle <= 123.75) {
		m_tFrame.iStateY = LEFTUP1125;
	}
	else if (123.75 <= m_fAngle && m_fAngle <= 146.25) {
		m_tFrame.iStateY = LEFTUP135;
	}
	else if (146.25 <= m_fAngle && m_fAngle <= 168.75) {
		m_tFrame.iStateY = LEFTUP1575;
	}
	else if ((168.75 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle <= -168.75)) {
		m_tFrame.iStateY = LEFT;
	}
	else if (-168.75 <= m_fAngle && m_fAngle <= -146.25) {
		m_tFrame.iStateY = LEFTDOWN1575;
	}
	else if (-146.25 <= m_fAngle && m_fAngle <= -123.75) {
		m_tFrame.iStateY = LEFTDOWN135;
	}
	else if (-123.75 <= m_fAngle && m_fAngle <= -101.25) {
		m_tFrame.iStateY = LEFTDOWN1125;
	}
	else if (-101.25 <= m_fAngle && m_fAngle <= -78.75) {
		m_tFrame.iStateY = LEFTDOWN;
	}
	else if (-78.75 <= m_fAngle && m_fAngle <= -56.25) {
		m_tFrame.iStateY = RIGHTDOWN675;
	}
	else if (-56.25 <= m_fAngle && m_fAngle <= -33.75) {
		m_tFrame.iStateY = RIGHTDOWN45;
	}
	else if (-33.75 <= m_fAngle && m_fAngle <= -11.25) {
		m_tFrame.iStateY = RIGHTDOWN225;
	}
	else if ((-11.25 <= m_fAngle && m_fAngle <= 0)) {
		m_tFrame.iStateY = RIGHT;
	}
}

void CBow::Key_State() {

}