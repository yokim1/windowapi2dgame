#pragma once
#include "Obj.h"
class CArcherAttack : public CObj
{
public:
	//enum RIGHT_STATE { RIGHT_UP, RIGHTUP675, RIGHTUP45, RIGHTUP225, RIGHT, RIGHTDOWN225, RIGHTDOWN45, RIGHTDOWN675, RIGHTDOWN, END };
	//enum LEFT_STATE {LEFT_UP, LEFTUP1125, LEFTUP135, LEFTUP1575, LEFT, LEFTDOWN1375,LEFTDOWN135, LEFTDOWN1125, LEFTDOWN, END};

	enum STATE {UP, UP675, UP45, UP225, SIDE, DOWN225, DOWN45, DOWN675, DOWN};
public:
	CArcherAttack();
	~CArcherAttack();


public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	void Set_State();
	void Update_Frame();

	void Update_HitBox() override;

public:
	void Set_Dir(BULLET::DIR _eDir) { m_eDir = _eDir; }

private:
	BULLET::DIR		m_eDir;

	float			playerFX;
	float			playerFY;

	float			fDistanceBetweenSlashAndPlayerX;
	float			fDistanceBetweenSlashAndPlayerY;

	STATE m_eCurState;
	STATE m_ePreState;

	bool m_bShot;
	/*
	RIGHT_STATE rs;
	LEFT_STATE ls;*/
};

