#pragma once
#include "Obj.h"
class CBow : public CObj
{
public:
	enum STATE { ATTACK, END };

	enum DIR_STATE_RIGHT {
		RIGHTUP, RIGHTUP675, RIGHTUP45, RIGHTUP225, RIGHT,
		RIGHTDOWN225,RIGHTDOWN45,RIGHTDOWN675, RIGHTDOWN };
	
	enum DIR_STATE_LEFT {
		LEFTUP, LEFTUP1125, LEFTUP135, LEFTUP1575, LEFT,
		LEFTDOWN1575, LEFTDOWN135, LEFTDOWN1125, LEFTDOWN };

public:
	CBow();
	virtual ~CBow();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	template <typename T>
	CObj* Create_Bullet()
	{
		CObj* pObj = CAbstractFactory<T>::Create(m_tInfo.fX, m_tInfo.fY, m_fAngle);

		return pObj;
	}

private:
	//POINT		m_tPosin;

	void Update_Frame();
	void Set_State();

	void Update_Range();
	void Update_Direction();
	void Key_State();

private:
	template <typename T>
	CObj* Create_Bullet(float fX, float fY)
	{
		CObj* pObj = CAbstractFactory<T>::Create(fX, fY, m_fAngle);

		return pObj;
	}

private:
	//float	m_fRadius;
	RECT	m_tRangeRect;
	RECT	m_tAttackRangeRect;

	STATE	m_eCurState;
	STATE	m_ePreState;
};

