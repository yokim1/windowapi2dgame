#include "stdafx.h"
#include "CWizardBall.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CSwordManAttack.h"


CWizardBall::CWizardBall()
{
}


CWizardBall::~CWizardBall()
{
}


void CWizardBall::Initialize() {

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WIZARD_BALL.bmp", L"WIZARD_BALL");

	m_tInfo.iCX = 50;
	m_tInfo.iCY = 50;

	m_fSpeed = 1.f;

	m_eRenderID = RENDERID::OBJECT;

	m_pFrameKey = L"WIZARD_BALL";
}

int CWizardBall::Update() {
	if (m_bDead)
		return OBJ_DEAD;

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };

	//bCheck_Rect의 parameter를 충돌 Rect들로 바꿔야한다.
	if (m_bHit) {
		m_eCurState = HIT_IDLE;
		m_tInfo.iHP -= 5;
		m_bHit = false;
	}
	else if (PtInRect(&m_tRangeRect, pt) && !CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		//	fRad = 2 * PI - fRad;
		//m_fAngle = fRad * 180.f / PI;

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
		m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

		if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
			m_eCurState = RIGHT;
		else
			m_eCurState = LEFT;
	}
	else if (CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		m_eCurState = (STATE)Update_Direction();
	}
	//else
	//{
	//	m_eCurState = HIT_IDLE;
	//}

	Set_State();

	Update_Rect();
	Update_Range();

	Update_Frame();
	Update_Direction();

	Key_State();

	return OBJ_NOEVENT;
}

void CWizardBall::Late_Update() {

}


void CWizardBall::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	//Range
	//Ellipse(_DC, m_tRangeRect.left + iScrollX, m_tRangeRect.top + iScrollY, m_tRangeRect.right + iScrollX, m_tRangeRect.bottom + iScrollY);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 90, m_tFrame.iStateY * 90
		, 90, 90
		, RGB(255, 0, 255));
}

void CWizardBall::Release() {

}


void CWizardBall::Update_Frame() {
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			if (m_eCurState != HIT_IDLE) {
				m_eCurState = HIT_IDLE;
			}

			m_tFrame.iStartX = 0;
		}
	}
}

void CWizardBall::Set_State() {
	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState)
		{
		case CWizardBall::HIT_IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 3;
			m_tFrame.iStateY = HIT_IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		default:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.dwSpeed = 500;
			m_tFrame.dwTime = GetTickCount();
			break;
		}
		m_ePreState = m_eCurState;
	}
}

int CWizardBall::Update_Direction() {

	if ((0 <= m_fAngle && m_fAngle <= 11.25)) {
		m_tFrame.iStateY = RIGHT;
	}
	else if (11.25 <= m_fAngle && m_fAngle <= 33.75) {
		m_tFrame.iStateY = RIGHTUP225;
	}
	else if (33.75 <= m_fAngle && m_fAngle <= 56.25) {
		m_tFrame.iStateY = RIGHTUP45;
	}
	else if (56.25 <= m_fAngle && m_fAngle <= 78.75) {
		m_tFrame.iStateY = RIGHTUP675;
	}
	else if (78.75 <= m_fAngle && m_fAngle <= 101.25) {
		m_tFrame.iStateY = UP;
	}
	else if (101.25 <= m_fAngle && m_fAngle <= 123.75) {
		m_tFrame.iStateY = LEFTUP1125;
	}
	else if (123.75 <= m_fAngle && m_fAngle <= 146.25) {
		m_tFrame.iStateY = LEFTUP135;
	}
	else if (146.25 <= m_fAngle && m_fAngle <= 168.75) {
		m_tFrame.iStateY = LEFTUP1575;
	}
	else if ((168.75 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle <= -168.75)) {
		m_tFrame.iStateY = LEFT;
	}
	else if (-168.75 <= m_fAngle && m_fAngle <= -146.25) {
		m_tFrame.iStateY = LEFTDOWN1575;
	}
	else if (-146.25 <= m_fAngle && m_fAngle <= -123.75) {
		m_tFrame.iStateY = LEFTDOWN135;
	}
	else if (-123.75 <= m_fAngle && m_fAngle <= -101.25) {
		m_tFrame.iStateY = LEFTDOWN1125;
	}
	else if (-101.25 <= m_fAngle && m_fAngle <= -78.75) {
		m_tFrame.iStateY = DOWN;
	}
	else if (-78.75 <= m_fAngle && m_fAngle <= -56.25) {
		m_tFrame.iStateY = RIGHTDOWN675;
	}
	else if (-56.25 <= m_fAngle && m_fAngle <= -33.75) {
		m_tFrame.iStateY = RIGHTDOWN45;
	}
	else if (-33.75 <= m_fAngle && m_fAngle <= -11.25) {
		m_tFrame.iStateY = RIGHTDOWN225;
	}
	else if ((-11.25 <= m_fAngle && m_fAngle <= 0)) {
		m_tFrame.iStateY = RIGHT;
	}
	return m_tFrame.iStateY;
}


void CWizardBall::Update_Range() {
	m_tRangeRect.left = (LONG)(m_tInfo.fX - (500 >> 1));
	m_tRangeRect.top = (LONG)(m_tInfo.fY - (500 >> 1));
	m_tRangeRect.right = (LONG)(m_tInfo.fX + (500 >> 1));
	m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (500 >> 1));
}

void CWizardBall::Key_State() 
{
	/*if (m_eCurState == ATTACK && m_tFrame.iStartX == 1) {
		if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if ((45 <= m_fAngle && m_fAngle < 135)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
	}*/
}

void CWizardBall::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
}