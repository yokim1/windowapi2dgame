#include "stdafx.h"
#include "CCrystalIce.h"
#include "ObjMgr.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "CollisionMgr.h"
#include "SoundMgr.h"

CCrystalIce::CCrystalIce()
{
}


CCrystalIce::~CCrystalIce()
{
}

void CCrystalIce::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ICE_CRYSTAL.bmp", L"ICE_CRYSTAL");
	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = -4.f;

	m_fDis = 5.f;

	m_tFrame.iEndX = 18;

	m_iStartTime = (int)GetTickCount() / 1000;

	CSoundMgr::Get_Instance()->PlaySound(L"ICE_KRYSTAL_START.mp3", CSoundMgr::EFFECT);
}

int CCrystalIce::Update() {

	if ((((int)GetTickCount() / 1000) - m_iStartTime) >= m_iDuration) {
		Set_Dead();
	}

	if (m_bDead)
		return OBJ_DEAD;

	//계속 +=을 해서
	//Set_State()에서 설정 해놓은 범위 안에 들기 위해,
	//%360을 해준다.
	//근데 -로는 가지 않는다.
	m_fAngle += m_fSpeed;
	m_fAngle = m_fAngle % 360;

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();


	if (m_tInfo.fY < m_pTarget->Get_INFO().fY) {
		//m_fAngle *= -1;
	}
	else if (m_pTarget->Get_INFO().fY < m_tInfo.fY) {

	}

	m_tInfo.fX = m_pTarget->Get_INFO().fX + cosf(m_fAngle * PI / 180.f) * m_fDis;
	m_tInfo.fY = m_pTarget->Get_INFO().fY - sinf(m_fAngle * PI / 180.f) * m_fDis;
	
	if (100.f > m_fDis) {
		++m_fDis;
	}

	Update_Rect();
	Set_State();
	Update_Frame();

	return OBJ_NOEVENT;
}

void CCrystalIce::Late_Update() {

}

void CCrystalIce::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"ICE_CRYSTAL");

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 200, m_tFrame.iStateY * 200
		, 200, 200
		, RGB(200, 230, 250));
}

void CCrystalIce::Release() {

}

void CCrystalIce::Set_State() {
	
}

void CCrystalIce::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			m_tFrame.iStartX = 0;
		}
	}
}

void CCrystalIce::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY;
}