#include "stdafx.h"
#include "CMiddleBossUI.h"
#include "BmpMgr.h"
#include "ObjMgr.h"

CMiddleBossUI::CMiddleBossUI()
{
}


CMiddleBossUI::~CMiddleBossUI()
{
}


void CMiddleBossUI::Initialize() {

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MIDDLEBOSS_NAMEBAR.bmp", L"MIDDLEBOSS_NAMEBAR");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_HPBAR.bmp", L"UI_HPBAR");

	m_eRenderID = RENDERID::UI;

	m_tInfo.fX = 100.f;
	m_tInfo.fY = 600.f;

	m_tInfo.iCX = 210; //280;//64;
	m_tInfo.iCY = 66;// 88;//16;

	m_iHp = 166;
}

int CMiddleBossUI::Update() {
	if (m_bDead) {
		return OBJ_DEAD;
	}

	m_pTarget = CObjMgr::Get_Instance()->Get_MiddleBoss();
	m_iHp = m_pTarget->Get_INFO().iHP;

	Update_Rect();

	return OBJ_NOEVENT;
}

void CMiddleBossUI::Late_Update() {

}

void CMiddleBossUI::Render(HDC _DC) {

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"MIDDLEBOSS_NAMEBAR");
	GdiTransparentBlt(_DC
		, 300, 20
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, 0, 0
		, 455, 90
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_HPBAR");
	GdiTransparentBlt(_DC
		, 320, 50
		, m_iHp, 22
		, hMemDC
		, 0, 0
		, 244, 32
		, RGB(255, 0, 255));
}

void CMiddleBossUI::Release() {

}
