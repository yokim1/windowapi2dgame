#include "stdafx.h"
#include "CScrewBullet2.h"
#include "ObjMgr.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "CollisionMgr.h"
#include "SoundMgr.h"

CScrewBullet2::CScrewBullet2()
{

}


CScrewBullet2::~CScrewBullet2()
{
	Release();
}

void CScrewBullet2::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SKILL_FIREDRAGON_COM.bmp", L"SKILL_FIREDRAGON_COM");

	isBegin = true;

	m_tInfo.iCX = 60;
	m_tInfo.iCY = 60;

	m_fSpeed = 10.f;

	m_fCenterBulletSpeed = 20.f;

	begin = NULL;
	m_tFrame.iStartX = NULL;
	end = NULL;

	isBack = true;
	isCreated = false;
	//CSoundMgr::Get_Instance()->StopSound(CSoundMgr::FIREDRAGON);
	CSoundMgr::Get_Instance()->PlaySound(L"FIRE_DRAGON_1.mp3", CSoundMgr::FIREDRAGON);
}


int CScrewBullet2::Update()
{
	if (1 * TILECX >= m_tInfo.fX || 1 * TILECY >= m_tInfo.fY
		|| 25 * TILECX <= (m_tInfo.fX + 50) || 31 * TILECY <= (m_tInfo.fY + 50))
		m_bDead = true;

	if (m_bDead)
		return OBJ_DEAD;

	if (isBegin) {
		m_tCenterBullet.fX = m_tInfo.fX;
		m_tCenterBullet.fY = m_tInfo.fY;

		m_fCenterBulletAngle = m_fAngle;
		isBegin = false;
	}

	//centerBullet movement
	m_tCenterBullet.fX += cosf(m_fCenterBulletAngle * 3.14 / 180) * m_fSpeed;
	m_tCenterBullet.fY -= sinf(m_fCenterBulletAngle * 3.14 / 180) * m_fSpeed;

	//around Bullet movement around centerBullet
	m_tInfo.fX = m_tCenterBullet.fX + cosf(m_fAngle * 3.14 / 180.f) * 30.f;
	m_tInfo.fY = m_tCenterBullet.fY - sinf(m_fAngle * 3.14 / 180.f) * 30.f;
	m_fAngle -= m_fSpeed;
	CSoundMgr::Get_Instance()->PlaySound(L"FIRE_DRAGON_1.mp3", CSoundMgr::FIREDRAGON);

	Update_Angle();
	
	Update_HitBox();
	Update_Rect();
	Update_Frame();

	return OBJ_NOEVENT;
}

void CScrewBullet2::Late_Update()
{
	/*if (100 >= m_tRect.left || 100 >= m_tRect.top
		|| WINCX - 100 <= m_tRect.right || WINCY - 100 <= m_tRect.bottom)
		m_bDead = true;*/
}

void CScrewBullet2::Render(HDC _DC)
{
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"SKILL_FIREDRAGON_COM");

	float fScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	float fScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tRect.left + fScrollX, m_tRect.top + fScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 180, m_tFrame.iStateY * 180
		, 180, 180
		, RGB(255, 0, 255));

}

void CScrewBullet2::Release()
{
}

void CScrewBullet2::Update_Frame()
{
	if (isBack == false) {
		if (m_tFrame.dwTime + 100 < GetTickCount())
		{
			++m_tFrame.iStartX;
			m_tFrame.dwTime = GetTickCount();

			if (m_tFrame.iStartX >= end)
			{
				//m_tFrame.iStartX = 0;
				m_tFrame.iStartX = end;
				isBack = true;
			}
		}
	}
	else
	{
		if (m_tFrame.dwTime + 100 < GetTickCount())
		{
			--m_tFrame.iStartX;
			m_tFrame.dwTime = GetTickCount();

			if (m_tFrame.iStartX <= begin)
			{
				m_tFrame.iStartX = begin;
				isBack = false;
			}
		}
	}

}

void CScrewBullet2::Update_Rect()
{
	m_tRect.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 1));
	m_tRect.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1));
	m_tRect.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 1));
	m_tRect.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1));
}

void CScrewBullet2::Update_Angle() {

	if (isCreated == false) {
		if (0 <= m_fAngle && m_fAngle <= 22.5) {
			begin = 9;
			m_tFrame.iStartX = 10;
			end = 11;
			m_tFrame.iStateY = 0;
		}
		else if (22.5 <= m_fAngle && m_fAngle <= 67.5) {
			begin = 12;
			m_tFrame.iStartX = 13;
			end = 14;
			m_tFrame.iStateY = 0;
		}
		else if (67.5 <= m_fAngle && m_fAngle <= 112.5) {
			begin = 3;
			m_tFrame.iStartX = 4;
			m_tFrame.iStateY = 1;
			end = 5;
		}
		else if (112.5 <= m_fAngle && m_fAngle <= 157.5) {
			begin = 0;
			m_tFrame.iStartX = 2;
			m_tFrame.iStateY = 1;
			end = 3;
		}
		else if (157.5 <= m_fAngle && m_fAngle <= 180) {
			begin = 9;
			m_tFrame.iStartX = 10;
			m_tFrame.iStateY = 1;
			end = 11;
		}
		else if (-180 <= m_fAngle && m_fAngle <= -157.5) {
			begin = 9;
			m_tFrame.iStartX = 10;
			m_tFrame.iStateY = 1;
			end = 11;
		}
		else if (-157.5 <= m_fAngle && m_fAngle <= -112.5) {
			begin = 0;
			m_tFrame.iStartX = 1;
			m_tFrame.iStateY = 0;
			end = 2;
		}
		else if (-112.5 <= m_fAngle && m_fAngle <= -67.5) {
			begin = 3;
			m_tFrame.iStartX = 4;
			m_tFrame.iStateY = 0;
			end = 5;
		}
		else if (-67.5 <= m_fAngle && m_fAngle <= -22.5) {
			begin = 6;
			m_tFrame.iStartX = 7;
			m_tFrame.iStateY = 0;
			end = 8;
		}
		else if (-22.5 <= m_fAngle && m_fAngle <= 0) {
			begin = 9;
			m_tFrame.iStartX = 10;
			m_tFrame.iStateY = 0;
			end = 11;
		}
		isCreated = true;
	}
}

void CScrewBullet2::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
}