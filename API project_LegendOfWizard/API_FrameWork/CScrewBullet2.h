#pragma once

#include "Obj.h"
class CScrewBullet2 : public CObj
{
public:
	CScrewBullet2();
	~CScrewBullet2();

	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC) override;
	virtual void Release() override;

	void Update_Frame();
	void Update_Rect();
	void Update_Angle();

	void Update_HitBox() override;

private:
	int begin;
	int end;

	bool isBack;

	INFO m_tCenterBullet;

	float m_fCenterBulletSpeed;
	float m_fCenterBulletAngle;
	bool isBegin;

	bool isCreated;
};
