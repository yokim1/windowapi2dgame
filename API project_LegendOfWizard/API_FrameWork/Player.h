#pragma once

#ifndef __PLAYER_H__
#define __PLAYER_H__


#include "Obj.h"
class CPlayer : public CObj
{
public:
	//enum STATE { IDLE, WALK, ATTACK, HIT, DEAD, END };
	enum STATEWizard{IDLE, WALK, DASH, BASICATTACK, SKILLATTACK, HIT, DEAD, END};

public:
	CPlayer();
	virtual ~CPlayer();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	void Key_Check();
	void Jumping();
	void OffSet();
	void Set_State();

	void Update_Frame();
	void Update_HitBox() override;

private:
	CObj* Create_Shield();

private:
	POINT		m_tPosin;

	/*STATE		m_eCurState;
	STATE		m_ePreState;*/

	STATEWizard	m_eCurState;
	STATEWizard	m_ePreState;

	float		m_fJumpPower;
	float		m_fJumpTime;
	float		m_fJumpY;
	bool		m_bJump;

private:
	template <typename T>
	CObj* Create_Bullet(int Angle = 0)
	{
		//CObj* pObj = CAbstractFactory<T>::Create((float)m_tPosin.x, (float)m_tPosin.y, Angle);
		CObj* pObj = CAbstractFactory<T>::Create((float)m_tInfo.fX, (float)m_tInfo.fY, Angle);
		return pObj;
	}

	template <typename T>
	CObj* Create_Bullet(int Angle, int speed)
	{
		//CObj* pObj = CAbstractFactory<T>::Create((float)m_tPosin.x, (float)m_tPosin.y, Angle);
		CObj* pObj = CAbstractFactory<T>::Create((float)m_tInfo.fX, (float)m_tInfo.fY, Angle, speed);
		return pObj;
	}

	template <typename T>
	CObj* Create_Bullet(bool isReverse, int Angle = 0)
	{
		//CObj* pObj = CAbstractFactory<T>::Create((float)m_tPosin.x, (float)m_tPosin.y, Angle);
		CObj* pObj = CAbstractFactory<T>::Create((float)m_tInfo.fX, (float)m_tInfo.fY, Angle, isReverse);
		return pObj;
	}

private:
	list<CObj*>* m_plistBullet;
	bool m_bATTACK;
	bool m_bInventory;

	int iStartTimeForShield;
	int iStartTimeForCrystalIce;
	
	bool isFireHead;
	int FireCount;

	bool isIceBlast;

	float m_fFixedAngle;

	CObj* m_pInventory;

	bool m_isTeleported;
};


#endif // !__PLAYER_H__
