#include "stdafx.h"
#include "Monster.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CSwordManAttack.h"
#include "SoundMgr.h"
#include "CCoin.h"
#include "CCountMgr.h"

CMonster::CMonster()
{
	ZeroMemory(&m_tPosin, sizeof(m_tPosin));
}


CMonster::~CMonster()
{
	Release();
}

void CMonster::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SWORDMAN_LEFT.bmp", L"SWORDMAN_LEFT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SWORDMAN_RIGHT.bmp", L"SWORDMAN_RIGHT");
	
	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_tInfo.iHP = 700;

	m_fSpeed = 3.f;

	m_bAttack = false;

	m_eRenderID = RENDERID::OBJECT;

	m_pFrameKey = L"SWORDMAN_LEFT";
	CCountMgr::Get_Instance()->IncreaseCount();

}

int CMonster::Update()
{
	if (m_bDead) {
		CObj* pObj = CAbstractFactory<CCoin>::Create(m_tInfo.fX, m_tInfo.fY);
		CObjMgr::Get_Instance()->Add_Object(OBJID::COIN, pObj);
		CSoundMgr::Get_Instance()->PlaySound(L"ENEMY_DIED_2.mp3", CSoundMgr::MONSTER);
		CCountMgr::Get_Instance()->DecreaseCount();
		return OBJ_DEAD;
	}
	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };

	if (m_tInfo.iHP <= 0) {
		m_eCurState = DEAD;
	}
	else if (m_bHit) {
		m_pTarget->IncreaseMp();
		m_eCurState = HIT;
		m_tInfo.iHP -= 5;
		m_bHit = false;
		CSoundMgr::Get_Instance()->PlaySound(L"HIT_SOUND_NORMAL_1.mp3", CSoundMgr::EFFECT);
	}
	else if (PtInRect(&m_tRangeRect, pt) && !CCollisionMgr::bCheck_HitBox(m_pTarget, this))//CCollisionMgr::bCheck_Rect(m_pTarget, this)) 
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);
		float fRad = acosf(fX / fDia);

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
		m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

		if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
			m_pFrameKey = L"SWORDMAN_RIGHT";
		else
			m_pFrameKey = L"SWORDMAN_LEFT";

		m_eCurState = WALK;
	}
	else if(CCollisionMgr::bCheck_HitBox(m_pTarget, this))
	{
		m_eCurState = ATTACK;
		
	}
	else 
	{
		m_eCurState = IDLE;
	}

	Set_State();

	Update_Rect();
	Update_Range();

	Update_Frame();

	Key_State();

	return OBJ_NOEVENT;
}

void CMonster::Late_Update()
{
	
}

void CMonster::Render(HDC _DC)
{
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);
	
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 200, m_tFrame.iStateY* 200
		, 200, 200
		, RGB(255, 0, 255));

	if (m_tFrame.iStartX == 1 && m_eCurState == ATTACK) {
		CSoundMgr::Get_Instance()->PlaySound(L"SWORDMAN_ATTACK.mp3", CSoundMgr::MONSTER);
	}
}

void CMonster::Release()
{
}

void CMonster::Key_State() 
{ 
	if (m_eCurState == ATTACK && m_tFrame.iStartX == 1 && m_bAttack == false) {
		if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if ((45 <= m_fAngle && m_fAngle < 135)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CSwordManAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		m_bAttack = true;
	}
}

void CMonster::Set_State()
{
	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState)
		{
		case CMonster::IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 0;
			m_tFrame.iStateY = IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMonster::WALK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 6;
			m_tFrame.iStateY = WALK;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMonster::ATTACK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 3;
			m_tFrame.iStateY = ATTACK;
			m_tFrame.dwSpeed = 550;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMonster::HIT:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = HIT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMonster::DEAD:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 6;
			m_tFrame.iStateY = DEAD;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;
		}
		m_ePreState = m_eCurState;
	}
}

void CMonster::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			if (m_eCurState == DEAD) {
				Set_Dead();
			}

			if (m_eCurState == ATTACK || m_eCurState == WALK) {
				m_eCurState = IDLE;
				m_bAttack = false;
			}

			m_tFrame.iStartX = 0;
		}
	}
}

void CMonster::Update_Range() 
{
	m_tRangeRect.left = (LONG)(m_tInfo.fX - (500 >> 1));
	m_tRangeRect.top = (LONG)(m_tInfo.fY - (500 >> 1));
	m_tRangeRect.right = (LONG)(m_tInfo.fX + (500 >> 1));
	m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (500 >> 1));
}

void CMonster::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY;
}