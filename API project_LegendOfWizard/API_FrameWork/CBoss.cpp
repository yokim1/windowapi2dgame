#include "stdafx.h"
#include "CBoss.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CWizardBall.h"
#include "CBossUI.h"

CBoss::CBoss()
{
}


CBoss::~CBoss()
{
}


void CBoss::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Boss/BOSS_LEFT.bmp", L"BOSS_LEFT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Boss/BOSS_RIGHT.bmp", L"BOSS_RIGHT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Boss/BOSS_ATTACK.bmp", L"BOSS_ATTACK");

	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = 1.f;

	m_eRenderID = RENDERID::OBJECT;

	m_pFrameKey = L"BOSS_LEFT";

	CObj* pObj = CAbstractFactory<CBossUI>::Create();
	CObjMgr::Get_Instance()->Add_Object(OBJID::UI, pObj);

	iAttackPattern = 0;
}

int CBoss::Update() {
	if (m_bDead)
		return OBJ_DEAD;

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };

	//bCheck_Rect의 parameter를 충돌 Rect들로 바꿔야한다.
	if (PtInRect(&m_tRangeRect, pt) && !CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		//	fRad = 2 * PI - fRad;
		//m_fAngle = fRad * 180.f / PI;

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
		m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

		if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
			m_pFrameKey = L"BOSS_RIGHT";
		else
			m_pFrameKey = L"BOSS_LEFT";

		m_eCurState = ATTACK1;
	}
	else if (CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		m_eCurState = ATTACK1;
	}
	else
	{
		m_eCurState = IDLE;
	}

	Set_State();

	Update_Rect();
	Update_Range();

	Update_Frame();

	Key_State();

	return OBJ_NOEVENT;
}

void CBoss::Late_Update() {

}

void CBoss::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	//Range
	//Ellipse(_DC, m_tRangeRect.left + iScrollX, m_tRangeRect.top + iScrollY, m_tRangeRect.right + iScrollX, m_tRangeRect.bottom + iScrollY);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 220, m_tFrame.iStateY * 300
		, 220, 300
		, RGB(255, 0, 255));

	if (m_eCurState == ATTACK1) {
		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"BOSS_ATTACK");
		
		GdiTransparentBlt(_DC
			, m_tRect.left + iScrollX, m_tRect.top + iScrollY
			, m_tInfo.iCX, m_tInfo.iCY
			, hMemDC
			, m_tFrame.iStartX * 220, m_tFrame.iStateY * 300
			, 220, 300
			, RGB(255, 0, 255));
	}
}

void CBoss::Release() {

}

void CBoss::Update_Frame() {

}
	
void CBoss::Set_State() {
	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState)
		{
		case CBoss::IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 0;
			m_tFrame.iStateY = IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CBoss::ATTACK1:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 9;
			m_tFrame.iStateY = ATTACK1;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CBoss::ATTACK2:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = ATTACK2;
			m_tFrame.dwSpeed = 550;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CBoss::ATTACK3:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 8;
			m_tFrame.iStateY = ATTACK3;
			m_tFrame.dwSpeed = 550;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CBoss::HIT:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = HIT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CBoss::DEAD:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 1;
			m_tFrame.iStateY = DEAD;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;
		}
		m_ePreState = m_eCurState;
	}
}

void CBoss::Update_Range() {
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			if (m_eCurState == ATTACK1 || m_eCurState == ATTACK2) {
				m_eCurState = IDLE;
			}

			m_tFrame.iStartX = 0;
		}
	}
}

//void CBoss::Update_Rect() {
//	m_tRangeRect.left = (LONG)(m_tInfo.fX - (500 >> 1));
//	m_tRangeRect.top = (LONG)(m_tInfo.fY - (500 >> 1));
//	m_tRangeRect.right = (LONG)(m_tInfo.fX + (500 >> 1));
//	m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (500 >> 1));
//}

void CBoss::Key_State() {
	if (m_eCurState == ATTACK1) {
		if (0 <= m_fAngle && m_fAngle <= 45) {
			m_tFrame.iStateY = 2;
		}
		else if (45 <= m_fAngle && m_fAngle <= 135) {
			m_tFrame.iStateY = 1;
		}
		else if (135 <= m_fAngle && m_fAngle <= 180) {
			m_tFrame.iStateY = 1;
		}
		else if (-180 <= m_fAngle && m_fAngle <= -135) {
			m_tFrame.iStateY = 1;
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			m_tFrame.iStateY = 3;
		}
		else if (-45 <= m_fAngle && m_fAngle <= 0) {
			m_tFrame.iStateY = 2;
		}
	}
}

void CBoss::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
}