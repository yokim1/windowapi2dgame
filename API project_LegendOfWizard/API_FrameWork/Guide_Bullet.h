#pragma once

#ifndef __GUIDE_BULLET_H__
#define __GUIDE_BULLET_H__


#include "Obj.h"
class CGuide_Bullet : public CObj
{
public:
	CGuide_Bullet();
	virtual ~CGuide_Bullet();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;
	
	void Update_HitBox() override;

	void Update_Frame();

private :
	bool m_bIsHit;
};


#endif // !__GUIDE_BULLET_H__
