#pragma once

#ifndef __SCREW_BULLET_H__
#define __SCREW_BULLET_H__


#include "Obj.h"
class CScrewBullet : public CObj
{
public:
	CScrewBullet();
	~CScrewBullet();

	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC) override;
	virtual void Release() override;

	void Update_Angle();
	void Update_Frame();
	void Update_Rect();

	void Update_HitBox() override;

private:
	int begin;
	int end;
	
	bool isBack;

	INFO m_tCenterBullet;

	float m_fCenterBulletSpeed;
	float m_fCenterBulletAngle;
	bool isBegin;

	bool isCreated;
};


#endif // !__SCREW_BULLET_H__
