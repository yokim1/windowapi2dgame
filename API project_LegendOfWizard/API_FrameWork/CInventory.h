#pragma once
#include "Obj.h"

class CObj;
class CInventory {
public:
	CInventory();
	virtual ~CInventory();

public:
	virtual void Initialize();
	virtual int Update();
	virtual void Late_Update();
	virtual void Render(HDC _DC);
	virtual void Release();

	void AddItem();

	bool Get_IsInventoryOpen() { return m_bInventory; }

public:
	static CInventory* Get_Instance()
	{
		if (!m_pInstance)
			m_pInstance = new CInventory;
		return m_pInstance;
	}
	static void Destroy_Instance()
	{
		SAFE_DELETE(m_pInstance);
	}

private:
	static CInventory*		m_pInstance;
	CObj* m_pItemArray[3];
	bool m_bInventory;

	RECT qItem;

	RECT eItem;

	RECT rItem;

	RECT qItemDescription;

	int iBoxSize = 20;
	int iDescriptionSize;
};