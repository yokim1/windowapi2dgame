#include "stdafx.h"
#include "CInventory.h"
#include "BmpMgr.h"
#include "KeyMgr.h"
#include "ObjMgr.h"
#include "Obj.h"
#include "SoundMgr.h"

CInventory* CInventory::m_pInstance = nullptr;

CInventory::CInventory()
{
}

CInventory::~CInventory()
{
}

void CInventory::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/INVENTORY.bmp", L"INVENTORY");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SELECT_ICON.bmp", L"SELECT_ICON");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/FIRE_DRAGON_ICON.bmp", L"FIRE_DRAGON_ICON");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/GAIA_ARMOR_ICON.bmp", L"GAIA_ARMOR_ICON");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ICE_KRYSTAL_ICON.bmp", L"ICE_KRYSTAL_ICON");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/FIRE_DRAGON_EX.bmp", L"FIRE_DRAGON_EX");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/GAIA_ARMOR_EX.bmp", L"GAIA_ARMOR_EX");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ICE_KRYStAL_EX.bmp", L"ICE_KRYSTAL_EX");

	m_bInventory = false;

	qItem.left = 252;
	qItem.top = 144;
	qItem.right = qItem.left + iBoxSize;
	qItem.bottom = qItem.top + iBoxSize;

	///////////////////////////////////////////////////////////////////

	eItem.left = 290;
	eItem.top = 144;
	eItem.right = eItem.left + iBoxSize;
	eItem.bottom = eItem.top + iBoxSize;

	///////////////////////////////////////////////////////////////////

	rItem.left = 328;
	rItem.top = 144;
	rItem.right = rItem.left + iBoxSize;
	rItem.bottom = rItem.top + iBoxSize;

	//////////////////////////////////////////////////////////////////

	qItemDescription.left = 125;
	qItemDescription.top = 365;
	qItemDescription.right = 365;
	qItemDescription.bottom = 515;

}

int CInventory::Update() {
	if (CKeyMgr::Get_Instance()->Key_Down(VK_TAB)) {
		m_bInventory = !m_bInventory;
		CSoundMgr::Get_Instance()->PlaySound(L"OPEN_INVENTORY.mp3", CSoundMgr::MONSTER);
	}
	return OBJ_NOEVENT;
}	

void CInventory::Late_Update() {

}

void CInventory::Render(HDC _DC) {

	POINT	pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);
	ShowCursor(true);

	if (m_bInventory) {

		HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"INVENTORY");
		GdiTransparentBlt(_DC
			, 100, 100
			, 288, 432//576, 865
			, hMemDC
			, 0, 0
			, 576, 865
			, RGB(255, 0, 255));

		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"NORMAL_ATTACK_ICON");
		GdiTransparentBlt(_DC
			, 212, 144
			, 25, 25//576, 865
			, hMemDC
			, 0, 0
			, 52, 52
			, RGB(255, 0, 255));


		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"FIRE_DRAGON_ICON");
		GdiTransparentBlt(_DC
			, qItem.left, qItem.top
			, iBoxSize, iBoxSize//576, 865
			, hMemDC
			, 0, 0
			, 52, 52
			, RGB(255, 0, 255));

		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"GAIA_ARMOR_ICON");
		GdiTransparentBlt(_DC
			, eItem.left, eItem.top
			, iBoxSize, iBoxSize//576, 865
			, hMemDC
			, 0, 0
			, 52, 52
			, RGB(255, 0, 255));

		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"ICE_KRYSTAL_ICON");
		GdiTransparentBlt(_DC
			, rItem.left, rItem.top
			, iBoxSize, iBoxSize//576, 865
			, hMemDC
			, 0, 0
			, 52, 52
			, RGB(255, 0, 255));

		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"SELECT_ICON");

		if (PtInRect(&qItem, pt)) {

			GdiTransparentBlt(_DC
				, qItem.left, qItem.top
				, iBoxSize, iBoxSize//576, 865
				, hMemDC
				, 0, 0
				, 52, 52
				, RGB(255, 0, 255));

			hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"FIRE_DRAGON_EX");
		}
		else if (PtInRect(&eItem, pt)) {
			GdiTransparentBlt(_DC
				, eItem.left, eItem.top
				, iBoxSize, iBoxSize	//576, 865
				, hMemDC
				, 0, 0
				, 52, 52
				, RGB(255, 0, 255));
			
			hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"GAIA_ARMOR_EX");
		}
		else if (PtInRect(&rItem, pt)) {
			GdiTransparentBlt(_DC
				, rItem.left, rItem.top
				, iBoxSize, iBoxSize	//576, 865
				, hMemDC
				, 0, 0
				, 52, 52
				, RGB(255, 0, 255));

			hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"ICE_KRYSTAL_EX");
		}

		GdiTransparentBlt(_DC
			, qItemDescription.left, qItemDescription.top
			, 240, 150	//576, 865
			, hMemDC
			, 0, 0
			, 475, 200
			, RGB(255, 0, 255));
	}

	//TCHAR lpOut[1000];
	//wsprintf(lpOut, TEXT("X: %d"), pt.x);
	//TextOut(_DC, 500, 100, lpOut, lstrlen(lpOut));

	//wsprintf(lpOut, TEXT("Y: %d"), pt.y);
	//TextOut(_DC, 500, 200, lpOut, lstrlen(lpOut));
}

void CInventory::Release() {

}

void CInventory::AddItem() {

}