#pragma once

#ifndef __SCENEMGR_H__
#define __SCENEMGR_H__

class CScene;
class CSceneMgr
{
public:
	enum SCENE { LOGO, MENU, EDIT, STAGE, END };

private:
	CSceneMgr();
	~CSceneMgr();

public:
	void Scene_Change(SCENE _eScene);
	void Update();
	void Late_Update();
	void Render(HDC _DC);
	void Release();

public:
	static CSceneMgr* Get_Instance()
	{
		if (!m_pInstance)
			m_pInstance = new CSceneMgr;
		return m_pInstance;
	}
	static void Destroy_Instance()
	{
		SAFE_DELETE(m_pInstance);
	}
	
private:
	static CSceneMgr*	m_pInstance;

	CScene*		m_pScene;

	SCENE		m_eCurScene;
	SCENE		m_ePreScene;
};


#endif // !__SCENEMGR_H__

