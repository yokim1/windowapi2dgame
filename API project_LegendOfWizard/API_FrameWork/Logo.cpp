#include "stdafx.h"
#include "Logo.h"
#include "BmpMgr.h"
#include "..\Editor\KeyMgr.h"
#include "SceneMgr.h"
#include "SoundMgr.h"

CLogo::CLogo()
{
}


CLogo::~CLogo()
{
	Release();
}

void CLogo::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/READY_MENU.bmp", L"Logo");
}

void CLogo::Update()
{
}

void CLogo::Late_Update()
{
	if (CKeyMgr::Get_Instance()->Key_Down(VK_RETURN))
	{
		CSceneMgr::Get_Instance()->Scene_Change(CSceneMgr::MENU);
		return;
	}
}

void CLogo::Render(HDC _DC)
{
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"Logo");

	StretchBlt(_DC, 0, 0, WINCX, WINCY, hMemDC, 0, 0, 1920, 1080, SRCCOPY);
	
}

void CLogo::Release()
{
}
