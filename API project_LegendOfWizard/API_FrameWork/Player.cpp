#include "stdafx.h"
#include "Player.h"
#include "Bullet.h"
#include "Shield.h"

#include "Guide_Bullet.h"
#include "Screw_Bullet.h"
#include "CScrewBullet2.h"
#include "CCrystalIce.h"
#include "CParticle.h"

#include "LineMgr.h"
#include "KeyMgr.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "SoundMgr.h"
#include "ObjMgr.h"
#include "CInventory.h"




CPlayer::CPlayer()
	: m_fJumpPower(0.f), m_fJumpTime(0.f), m_bJump(false), m_fJumpY(0.f), m_bATTACK(true)
	,m_eCurState(END),m_ePreState(END)
{
	ZeroMemory(&m_tPosin, sizeof(m_tPosin));
}

CPlayer::~CPlayer()
{
	Release();
}

void CPlayer::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Player/FRONT_COMPLETE.bmp", L"Player_Front");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Player/LEFT_COMPLETE.bmp", L"Player_Left");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Player/RIGHT_COMPLETE.bmp", L"Player_Right");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Player/BACK_COMPLETE.bmp", L"Player_Back");

	//CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/INVENTORY.bmp", L"INVENTORY");

	/*m_tInfo.fX = 400.f;
	m_tInfo.fY = 300.f;*/

	m_tInfo.iCX = 80;
	m_tInfo.iCY = 80;

	m_tInfo.iHP = 125;
	m_tInfo.iMP = 0; // 100

	m_fSpeed = 5.f;

	m_fDis = 100.f;

	m_fJumpPower = 50.f;

	m_eCurState = IDLE;

	m_pFrameKey = L"Player_Front";

	m_eRenderID = RENDERID::OBJECT;

	//iStartTime = (int)GetTickCount() / 1000;

	m_bInventory = false;

	isFireHead = true;
	FireCount = 0;

	isIceBlast = false;
}

int CPlayer::Update()
{
	/*if (m_bHit) {
		m_tInfo.iHP -= 10;
		m_bHit = false;
	}*/

	if (m_tInfo.iHP <= 0) {
		Set_Dead();
		CSoundMgr::Get_Instance()->PlaySound(L"PLAYER_DIE.mp3", CSoundMgr::PLAYER);
	}

	//if (m_bDead)
	//	return OBJ_DEAD;

	m_pTarget = CObjMgr::Get_Instance()->Get_Target(this, OBJID::MOUSE);

	if (m_pTarget)
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;
	}

	Key_Check();
	Jumping();
	OffSet();
	Set_State();
	
	Update_HitBox();
	Update_Frame();
	Update_Rect();

	if (m_bHit) {
		m_eCurState = HIT;
		m_tInfo.iHP--;
		m_bHit = false;
		if (m_tInfo.iMP <= 95) {
			m_tInfo.iMP += 1;
		}
		CSoundMgr::Get_Instance()->PlaySound(L"PLAYER_HITED_1.mp3", CSoundMgr::PLAYER);
	}

	return OBJ_NOEVENT;
}

void CPlayer::Late_Update()
{
	if (m_eCurState == DASH) {
		if (!lstrcmp(L"Player_Front", m_pFrameKey)) {
			m_tInfo.fY += m_fSpeed;
		}
		else if (!lstrcmp(L"Player_Left", m_pFrameKey)) {
			m_tInfo.fX -= m_fSpeed;
		}
		else if (!lstrcmp(L"Player_Right", m_pFrameKey)) {
			m_tInfo.fX += m_fSpeed;
		}
		else if (!lstrcmp(L"Player_Back", m_pFrameKey)) {
			m_tInfo.fY -= m_fSpeed;
		}
	}
}

void CPlayer::Render(HDC _DC)
{
	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	Update_Rect();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 180, m_tFrame.iStateY * 180
		, 180, 180
		, RGB(255, 0, 255));
}

void CPlayer::Release()
{
}

void CPlayer::Key_Check()
{
	int iCurrentTick = (int)GetTickCount() / 1000;

	if(!CInventory::Get_Instance()->Get_IsInventoryOpen()){
		if (isFireHead == false && FireCount <= 4 && (m_tFrame.dwTime + 50 < GetTickCount())) {
		CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CParticle>(false, m_fAngle));
		CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CParticle>(true, m_fAngle));

		++FireCount;

		if (FireCount == 4) {
			isFireHead = true;
			FireCount = 0;
		}

		if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
			m_pFrameKey = L"Player_Right";
		}
		else if ((45 <= m_fAngle && m_fAngle < 135)) {
			m_pFrameKey = L"Player_Back";
		}
		else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
			m_pFrameKey = L"Player_Left";
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			m_pFrameKey = L"Player_Front";
		}

		if (m_bATTACK) {
			m_eCurState = BASICATTACK;
		}
		else {
			m_eCurState = SKILLATTACK;
		}
		m_bATTACK = !m_bATTACK;
		}
		else if (CKeyMgr::Get_Instance()->Key_Pressing('A'))
		{
			m_tInfo.fX -= m_fSpeed;
			m_eCurState = WALK;
			m_pFrameKey = L"Player_Left";
//			CSoundMgr::Get_Instance()->PlaySound(L"SWORDMAN_ATTACK.mp3", CSoundMgr::PLAYER);
		}
		else if (CKeyMgr::Get_Instance()->Key_Pressing('D'))
		{
			m_tInfo.fX += m_fSpeed;
			m_eCurState = WALK;
			m_pFrameKey = L"Player_Right";
		}
		else if (CKeyMgr::Get_Instance()->Key_Pressing('W'))
		{
			m_tInfo.fY -= m_fSpeed;
			m_eCurState = WALK;
			m_pFrameKey = L"Player_Back";
		}
		else if (CKeyMgr::Get_Instance()->Key_Pressing('S'))
		{
			m_tInfo.fY += m_fSpeed;
			m_eCurState = WALK;
			m_pFrameKey = L"Player_Front";
		}
		//pressing을 KeyDown으로 바꿔야한다.
		else if (CKeyMgr::Get_Instance()->Key_Down(VK_SPACE))
		{
			if (!lstrcmp(L"Player_Front", m_pFrameKey)) {
				m_tInfo.fY += m_fSpeed;
				m_pFrameKey = L"Player_Front";
			}
			else if (!lstrcmp(L"Player_Left", m_pFrameKey)) {
				m_tInfo.fX -= m_fSpeed;
				m_pFrameKey = L"Player_Left";
			}
			else if (!lstrcmp(L"Player_Right", m_pFrameKey)) {
				m_tInfo.fX += m_fSpeed;
				m_pFrameKey = L"Player_Right";
			}
			else if (!lstrcmp(L"Player_Back", m_pFrameKey)) {
				m_tInfo.fY -= m_fSpeed;
				m_pFrameKey = L"Player_Back";
			}
			m_eCurState = DASH;

			CSoundMgr::Get_Instance()->PlaySound(L"DASH_1.mp3", CSoundMgr::PLAYER);
		}
		//pressing을 KeyDown으로 바꿔야한다.
		else if (CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON))
		{
			if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
				m_pFrameKey = L"Player_Right";
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CBullet>(m_fAngle));
			}
			else if ((45 <= m_fAngle && m_fAngle < 135)) {
				m_pFrameKey = L"Player_Back";
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CBullet>(m_fAngle));
			}
			else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
				m_pFrameKey = L"Player_Left";
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CBullet>(m_fAngle));
			}
			else if (-135 <= m_fAngle && m_fAngle <= -45) {
				m_pFrameKey = L"Player_Front";
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CBullet>(m_fAngle));
			}

			if (m_bATTACK) {
				m_eCurState = BASICATTACK;
			}
			else {
				m_eCurState = SKILLATTACK;
			}
			m_bATTACK = !m_bATTACK;

			//if (m_eCurState == BASIC_ATTACK) {
				
			//}
		}
		else if (CKeyMgr::Get_Instance()->Key_Down('Q') && ((iStartTimeForShield - iCurrentTick) != 10)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(0));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(45));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(90));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(135));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(180));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(-135));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(-90));
			CObjMgr::Get_Instance()->Add_Object(OBJID::SHIELD, Create_Bullet<CShield>(-45));

			iStartTimeForShield = (int)GetTickCount() / 1000;

			if (m_bATTACK) {
				m_eCurState = BASICATTACK;
			}
			else {
				m_eCurState = SKILLATTACK;
			}
			m_bATTACK = !m_bATTACK;
		}
		else if (CKeyMgr::Get_Instance()->Key_Down('E')) {
			if ((m_tInfo.iMP >= 95)) {
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(45, 20));
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(135, 20));
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(-135, 20));
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(-45, 20));

				CSoundMgr::Get_Instance()->PlaySound(L"ULT_USE.mp3", CSoundMgr::EFFECT);
				m_tInfo.iMP = 0;
			}
			else {
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(45));
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(135));
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(-135));
				CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CCrystalIce>(-45));
			}
			
			if (m_bATTACK) {
				m_eCurState = BASICATTACK;
			}
			else {
				m_eCurState = SKILLATTACK;
			}
			m_bATTACK = !m_bATTACK;

			m_tInfo.iMP += 5;
		}
		else if (isFireHead && CKeyMgr::Get_Instance()->Key_Down('F')) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CScrewBullet>(m_fAngle));
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CScrewBullet2>(m_fAngle));

			if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
				m_pFrameKey = L"Player_Right";
			}
			else if ((45 <= m_fAngle && m_fAngle < 135)) {
				m_pFrameKey = L"Player_Back";
			}
			else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
				m_pFrameKey = L"Player_Left";
			}
			else if (-135 <= m_fAngle && m_fAngle <= -45) {
				m_pFrameKey = L"Player_Front";
			}

			if (m_bATTACK) {
				m_eCurState = BASICATTACK;
			}
			else {
				m_eCurState = SKILLATTACK;
			}
			m_bATTACK = !m_bATTACK;

			isFireHead = false;

			m_tInfo.iMP += 5;

			//CSoundMgr::Get_Instance()->PlaySound(L"FIRE_DRAGON_3.mp3", CSoundMgr::FIREDRAGON);
		}
		else if (CKeyMgr::Get_Instance()->Key_Down('X')) {

			if (100 <= m_iCoin) {
				m_iCoin = m_iCoin - 100;
				m_tInfo.iHP = 125;
			}
			CSoundMgr::Get_Instance()->PlaySound(L"ULT_ON.mp3", CSoundMgr::EFFECT);
		}
		else if (CKeyMgr::Get_Instance()->Key_Down('T')) {
			RECT temp;
			RECT target = { 32.f*TILECX, 14 * TILECY, 34.f * TILECX, 16.f * TILECY };
			if (IntersectRect(&temp, &Get_Rect(), & target)) {
				m_isTeleported = true;

				m_tInfo.fX = 18.f * TILECX;
				m_tInfo.fY = 29.f * TILECY;

				CSoundMgr::Get_Instance()->PlaySound(L"teleport.mp3", CSoundMgr::EFFECT);
			}
		}
		else if (m_eCurState != BASICATTACK && m_eCurState != DASH &&
			m_eCurState != SKILLATTACK && m_eCurState != HIT)
			m_eCurState = IDLE;
	}


}

void CPlayer::Jumping()
{
	float fY = 0.f;
	bool bLineCol = CLineMgr::Get_Instance()->Collision_Line(m_tInfo.fX, &fY);

	if (m_bJump)
	{
		m_tInfo.fY = m_fJumpY - (m_fJumpPower * m_fJumpTime - 9.8f * m_fJumpTime * m_fJumpTime * 0.5f);
		//m_tInfo.fY = m_tInfo.fY - (m_fJumpPower * m_fJumpTime - 9.8f * m_fJumpTime * m_fJumpTime * 0.5f);
		m_fJumpTime += 0.2f;

		if (m_tInfo.fY > fY && bLineCol)
		{
			m_fJumpTime = 0.f;
			m_tInfo.fY = fY;
			m_bJump = false;
		}
	}
	else if (bLineCol)
		m_tInfo.fY = fY;

}

void CPlayer::OffSet()
{
	const int iOffSetX = WINCX >> 1;
	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();

	if (iOffSetX < (int)m_tInfo.fX + iScrollX)
		CScrollMgr::Get_Instance()->Set_ScrollX(iOffSetX - (m_tInfo.fX + iScrollX));

	if (iOffSetX >(int)m_tInfo.fX + iScrollX)
		CScrollMgr::Get_Instance()->Set_ScrollX(iOffSetX - (m_tInfo.fX + iScrollX));
	

	const int iOffSetY = WINCY >> 1;
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();
	
	if (iOffSetY < (int)m_tInfo.fY + iScrollY)
		CScrollMgr::Get_Instance()->Set_ScrollY(iOffSetY - (m_tInfo.fY + iScrollY));

	if (iOffSetY > (int)m_tInfo.fY + iScrollY)
		CScrollMgr::Get_Instance()->Set_ScrollY(iOffSetY - (m_tInfo.fY + iScrollY));
}

void CPlayer::Set_State()
{
	if (m_bDead == true) {
		m_eCurState = DEAD;
	}

	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState) 
		{
		case CPlayer::IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 0;
			m_tFrame.iStateY = IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CPlayer::WALK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 10;
			m_tFrame.iStateY = WALK;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CPlayer::DASH:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 8;
			m_tFrame.iStateY = DASH;
			m_tFrame.dwSpeed = 50;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CPlayer::BASICATTACK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 8;
			m_tFrame.iStateY = BASICATTACK;
			m_tFrame.dwSpeed = 50;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CPlayer::SKILLATTACK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 8;
			m_tFrame.iStateY = SKILLATTACK;
			m_tFrame.dwSpeed = 50;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CPlayer::HIT:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = HIT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CPlayer::DEAD:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 7;
			m_tFrame.iStateY = DEAD;
			m_tFrame.dwSpeed = 100;
			m_pFrameKey = L"Player_Front";
			m_tFrame.dwTime = GetTickCount();
		}
		m_ePreState = m_eCurState;
	}
}

CObj* CPlayer::Create_Shield()
{
	CObj*	pObj = CAbstractFactory<CShield>::Create(this);
	//pObj->Set_Target(this);

	return pObj;
}

void CPlayer::Update_Frame()
{
	if (m_tFrame.iStartX == 6 && m_eCurState == DEAD) {
		int a = 0;
		return;
	}

	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{	
			if (m_eCurState == BASICATTACK || m_eCurState == SKILLATTACK || m_eCurState == DASH) {
				m_eCurState = IDLE;
			}

			m_tFrame.iStartX = 0;
		}
	}
}

void CPlayer::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 3)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 3)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY;
}