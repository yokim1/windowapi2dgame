#pragma once
#include "Obj.h"
class CNpc :
	public CObj
{
public:
	CNpc();
	virtual ~CNpc();

public:
	virtual void Initialize();
	virtual int Update();
	virtual void Late_Update();
	virtual void Render(HDC _DC);
	virtual void Release();

};

