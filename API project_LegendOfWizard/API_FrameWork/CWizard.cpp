#include "stdafx.h"
#include "CWizard.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CWizardFire.h"
#include "CCoin.h"
#include "SoundMgr.h"
#include "CCountMgr.h"

CWizard::CWizard()
{

}

CWizard::~CWizard()
{
}


void CWizard::Initialize() {

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WIZARD_LEFT.bmp", L"WIZARD_LEFT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WIZARD_RIGHT.bmp", L"WIZARD_RIGHT");

	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_tInfo.iHP = 800;

	m_fSpeed = 1.f;

	m_bAttack = false;

	m_eRenderID = RENDERID::OBJECT;

	m_pFrameKey = L"WIZARD_LEFT";
	CCountMgr::Get_Instance()->IncreaseCount();

}

int CWizard::Update() {
	if (m_bDead) {
		CObj* pObj = CAbstractFactory<CCoin>::Create(m_tInfo.fX, m_tInfo.fY);
		CObjMgr::Get_Instance()->Add_Object(OBJID::COIN, pObj);
		CSoundMgr::Get_Instance()->PlaySound(L"ENEMY_DIED_3.mp3", CSoundMgr::MONSTER);
		CCountMgr::Get_Instance()->DecreaseCount();
		return OBJ_DEAD;
	}

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };
	RECT rcTemp = {};

	//bCheck_Rect의 parameter를 충돌 Rect들로 바꿔야한다.
	if (m_tInfo.iHP <= 0) {
		m_eCurState = DEAD;
	}
	else if (m_bHit) {
		m_pTarget->IncreaseMp();
		m_eCurState = HIT;
		m_tInfo.iHP -= 5;
		m_bHit = false;
		CSoundMgr::Get_Instance()->PlaySound(L"HIT_SOUND_NORMAL_2.mp3", CSoundMgr::EFFECT);
	}
	else if (IntersectRect(&rcTemp, &m_tAttackRangeRect, &m_pTarget->Get_Rect()))
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		//	fRad = 2 * PI - fRad;
		//m_fAngle = fRad * 180.f / PI;

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_eCurState = ATTACK;
		//m_bShot = true;
	}
	else if (PtInRect(&m_tRangeRect, pt) && !CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		//	fRad = 2 * PI - fRad;
		//m_fAngle = fRad * 180.f / PI;

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
		m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

		if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
			m_pFrameKey = L"WIZARD_RIGHT";
		else
			m_pFrameKey = L"WIZARD_LEFT";

		m_eCurState = WALK;
	}
	else
	{
		m_eCurState = IDLE;
	}

	Set_State();

	Update_Rect();
	Update_Range();

	Update_Frame();

	Key_State();

	return OBJ_NOEVENT;
}

void CWizard::Late_Update() {
	
}

void CWizard::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	//Range
	//Ellipse(_DC, m_tRangeRect.left + iScrollX, m_tRangeRect.top + iScrollY, m_tRangeRect.right + iScrollX, m_tRangeRect.bottom + iScrollY);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 170, m_tFrame.iStateY * 230
		, 170, 230
		, RGB(255, 0, 255));
}

void CWizard::Release() {
	
}


void CWizard::Update_Frame() {
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			if (m_eCurState == DEAD) {
				Set_Dead();
			}

			if (m_eCurState == ATTACK || m_eCurState == WALK) {
				m_eCurState = IDLE;
				m_bAttack = false;
			}

			m_tFrame.iStartX = 0;
		}
	}
}

void CWizard::Set_State() {
	if (m_ePreState != m_eCurState)
	{
		//enum STATEWizard{IDLE, WALK, DASH, BASICATTACK, SKILLATTACK, HIT, END};
		switch (m_eCurState)
		{
		case CWizard::IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 0;
			m_tFrame.iStateY = IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CWizard::WALK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 3;
			m_tFrame.iStateY = WALK;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CWizard::ATTACK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 3;
			m_tFrame.iStateY = ATTACK;
			m_tFrame.dwSpeed = 500;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CWizard::HIT:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = HIT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CWizard::DEAD:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = DEAD;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;
		}
		m_ePreState = m_eCurState;
	}
}


void CWizard::Update_Range() {
	/*m_tRangeRect.left = (LONG)(m_tInfo.fX - (500 >> 1));
	m_tRangeRect.top = (LONG)(m_tInfo.fY - (500 >> 1));
	m_tRangeRect.right = (LONG)(m_tInfo.fX + (500 >> 1));
	m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (500 >> 1));
*/

		m_tRangeRect.left = (LONG)(m_tInfo.fX - (600 >> 1));
		m_tRangeRect.top = (LONG)(m_tInfo.fY - (600 >> 1));
		m_tRangeRect.right = (LONG)(m_tInfo.fX + (600 >> 1));
		m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (600 >> 1));

		m_tAttackRangeRect.left = (LONG)(m_tInfo.fX - (300 >> 1));
		m_tAttackRangeRect.top = (LONG)(m_tInfo.fY - (300 >> 1));
		m_tAttackRangeRect.right = (LONG)(m_tInfo.fX + (300 >> 1));
		m_tAttackRangeRect.bottom = (LONG)(m_tInfo.fY + (300 >> 1));

}

void CWizard::Key_State() {
	if (m_eCurState == ATTACK && m_tFrame.iStartX == 1 && m_bAttack == false) {
		CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>());
		m_bAttack = true;
	}
}

void CWizard::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
}