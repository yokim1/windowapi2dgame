#include "stdafx.h"
#include "CCoin.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "ObjMgr.h"
#include "SoundMgr.h"

CCoin::CCoin()
{
}


CCoin::~CCoin()
{
}


void CCoin::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MONEY.bmp", L"MONEY");

	value = (rand() % 50) + 1;

	m_eRenderID = RENDERID::OBJECT;

	m_tInfo.iCX = 15;
	m_tInfo.iCY = 15;

	m_iCoin = (rand() % 50) + 1;

	m_fSpeed = 7.f;
} 

int CCoin::Update() {
	if (m_bDead)
	{
		m_pTarget->Set_Coin(m_iCoin);
		CSoundMgr::Get_Instance()->PlaySound(L"MONEY_TAKE.mp3", CSoundMgr::MONSTER);
		return OBJ_DEAD;
	}

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
	float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;

	float fDia = sqrtf(fX * fX + fY * fY);

	float fRad = acosf(fX / fDia);

	m_fAngle = fRad * 180.f / PI;

	if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		m_fAngle *= -1.f;

	m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
	m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;


	Update_Rect();

	Update_Frame();
	return OBJ_NOEVENT;
}

void CCoin::Late_Update() {

}

 void CCoin::Render(HDC _DC) {

	 HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"MONEY");

	 int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	 int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	 GdiTransparentBlt(_DC
		 , m_tRect.left + iScrollX, m_tRect.top + iScrollY
		 , m_tInfo.iCX, m_tInfo.iCY
		 , hMemDC
		 , m_tFrame.iStartX *24 , m_tFrame.iStateY * 24
		 , 24, 24
		 , RGB(255, 0, 255));
}

void CCoin::RenderHitBox(HDC _DC) {

}

void CCoin::Release() {

}

void CCoin::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			m_tFrame.iStartX = 0;
		}
	}
}