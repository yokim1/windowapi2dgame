#pragma once

#ifndef __OBJMGR_H__
#define __OBJMGR_H__

class CObj;
class CObjMgr
{
private:
	CObjMgr();
	~CObjMgr();

public:
	void Add_Object(OBJID::ID _eID, CObj* _pObj);
	void Update();
	void Late_Update();
	void Render(HDC _DC);
	void Release();

	void KeyCheck();

public:
	CObj* Get_Player() { return m_listObj[OBJID::PLAYER].front(); }
	CObj* Get_SwordMan() { return m_listObj[OBJID::MONSTER].front(); }
	CObj* Get_MiddleBoss() { return m_listObj[OBJID::MIDDLEBOSS].front(); }
	CObj* Get_MiddleBossUI() { return m_listObj[OBJID::MIDDLEBOSSUI].front(); }

	CObj* Get_Mouse() { return m_listObj[OBJID::MOUSE].front(); }
	CObj* Get_Target(CObj* _pObj, OBJID::ID _eID);

public:
	void Delete_ID(OBJID::ID _eID);

public:
	static CObjMgr* Get_Instance()
	{
		if (!m_pInstance)
			m_pInstance = new CObjMgr;
		return m_pInstance;
	}
	static void Destroy_Instance()
	{
		SAFE_DELETE(m_pInstance);
	}

private:
	list<CObj*>			m_listObj[OBJID::END];
	list<CObj*>			m_listRender[RENDERID::END];
	static CObjMgr*		m_pInstance;

	bool isHitBoxRender;

	bool m_IsImmortal;
};


#endif // !__OBJMGR_H__
