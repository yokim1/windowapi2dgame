#include "stdafx.h"
#include "Mouse.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"

CMouse::CMouse()
{
}


CMouse::~CMouse()
{
	Release();
}

void CMouse::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_MOUSE.bmp", L"UI_MOUSE");

	m_tInfo.iCX = 60;
	m_tInfo.iCY = 60;
}

int CMouse::Update()
{
	if (m_bDead)
		return OBJ_DEAD;

	POINT	pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);
	ShowCursor(FALSE);


	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	pt.x -= iScrollX;
	pt.y -= iScrollY;

	m_tInfo.fX = (float)pt.x;
	m_tInfo.fY = (float)pt.y;

	Update_Rect();
	return OBJ_NOEVENT;
}

void CMouse::Late_Update()
{
}

void CMouse::Render(HDC _DC)
{
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_MOUSE");

	Update_Rect();

	POINT	pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);

	GdiTransparentBlt(_DC
		, pt.x - 15, pt.y - 15
		, m_tInfo.iCX >> 1, m_tInfo.iCY >> 1
		, hMemDC
		, 0, 0
		, 60, 60
		, RGB(255, 0, 255));
}

void CMouse::Release()
{
}
