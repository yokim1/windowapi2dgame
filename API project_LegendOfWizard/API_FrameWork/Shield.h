#pragma once

#ifndef __SHIELD_H__
#define __SHIELD_H__


#include "Obj.h"
class CShield : public CObj
{
//	enum STATE {DOWN, ,RIGHT, UP, LEFT};
public:
	CShield();
	virtual ~CShield();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	void Set_State();

	void Update_HitBox() override;

private:
	int m_iStartTime;
	const int m_iDuration = 10;
};


#endif // !__SHIELD_H__
