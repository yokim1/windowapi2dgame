#pragma once
#include "Obj.h"
class CArcher : public CObj
{
public:
	enum STATE { IDLE, WALK, ATTACK, HIT, DEAD, END };

public:
	CArcher();
	virtual ~CArcher();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	//POINT		m_tPosin;

	void Update_Frame();
	void Set_State();

	void Update_Range();
	void Key_State();

	void Update_HitBox() override;

private:
	template <typename T>
	CObj* Create_Bullet( )
	{
		CObj* pObj = CAbstractFactory<T>::Create(m_tInfo.fX, m_tInfo.fY, m_fAngle);

		return pObj;
	}

private:
	RECT	m_tRangeRect;
	RECT	m_tAttackRangeRect;

	STATE	m_eCurState;
	STATE	m_ePreState;

	CObj* pObj;

	bool m_bShot;

	int count;
};

