#include "stdafx.h"
#include "CSwordManAttack.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "ObjMgr.h"

CSwordManAttack::CSwordManAttack()
	: m_eDir(BULLET::END), m_eCurState(END), m_ePreState(END)
{

}


CSwordManAttack::~CSwordManAttack()
{
	Release();
}

void CSwordManAttack::Initialize()
{
	m_eRenderID = RENDERID::EFFECT;

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SWORDMAN_ATTACK.bmp", L"SWORDMAN_ATTACK");

	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = 5.f;

	m_fDis = 100.f;

	m_eCurState = END;

	//m_pTarget = CObjMgr::Get_Instance()->Get_SwordMan();
	//playerFX = m_tInfo.fX;
	//playerFY = m_tInfo.fX;
}

int CSwordManAttack::Update()
{
	if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
		m_tFrame.iStateY = RIGHT;
		m_eCurState = RIGHT;
	}
	else if ((45 <= m_fAngle && m_fAngle < 135)) {
		m_tFrame.iStateY = UP;
		m_eCurState = UP;
	}
	else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
		m_tFrame.iStateY = LEFT;
		m_eCurState = LEFT;
	}
	else if (-135 <= m_fAngle && m_fAngle <= -45) {
		m_tFrame.iStateY = DOWN;
		m_eCurState = DOWN;
	}

	if (m_bDead && m_tFrame.iStartX >= m_tFrame.iEndX)
		return OBJ_DEAD;

	m_tInfo.fX = m_tInfo.fX + cosf(m_fAngle * PI / 180.f); //* m_fSpeed;
	m_tInfo.fY = m_tInfo.fY - sinf(m_fAngle * PI / 180.f);//- 20.f; //* m_fSpeed;

	Set_State();
	Update_Frame();
	Update_Rect();
	Update_HitBox();

	return OBJ_NOEVENT;
}

void CSwordManAttack::Late_Update()
{
	
	Set_State();
}

void CSwordManAttack::Render(HDC _DC)
{
	Update_Rect();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"SWORDMAN_ATTACK");

	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tInfo.fX + iScrollX + fDistanceBetweenSlashAndPlayerX
		, m_tInfo.fY + iScrollY + fDistanceBetweenSlashAndPlayerY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 200, m_tFrame.iStateY * 200
		, 200, 200
		, RGB(255, 0, 255));
}

void CSwordManAttack::Release()
{
}

void CSwordManAttack::Set_State() {

	if (m_ePreState != m_eCurState)
	{
		if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = RIGHT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -50;
			fDistanceForHitBoxX = 23;
			fDistanceForHitBoxY = 0;
			
		}
		else if ((45 <= m_fAngle && m_fAngle < 135)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = UP;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -50;
			fDistanceBetweenSlashAndPlayerY = -80;
			fDistanceForHitBoxX = 0;
			fDistanceForHitBoxY = -23;
		}
		else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = LEFT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -90;
			fDistanceBetweenSlashAndPlayerY = -50;
			fDistanceForHitBoxX = -23;
			fDistanceForHitBoxY = 0;
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = DOWN;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -50;
			fDistanceBetweenSlashAndPlayerY = 10;
			fDistanceForHitBoxX = 0;
			fDistanceForHitBoxY = 30;
		}

		m_ePreState = m_eCurState;
	}
}

void CSwordManAttack::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			m_bDead = true;
			//m_tFrame.iStartX = 0;
		}
	}
}

void CSwordManAttack::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX + fDistanceForHitBoxX;//fDistanceBetweenSlashAndPlayerX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY + fDistanceForHitBoxY;//fDistanceBetweenSlashAndPlayerY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX + fDistanceForHitBoxX;//fDistanceBetweenSlashAndPlayerX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY + fDistanceForHitBoxY;//fDistanceBetweenSlashAndPlayerY;
}