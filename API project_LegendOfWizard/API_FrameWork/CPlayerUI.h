#pragma once
#include "Obj.h"

class CPlayerUI : public CObj
{
public:
	CPlayerUI();
	virtual ~CPlayerUI();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	int m_iHp;
	int m_iMp;

	RECT qItem;
	RECT eItem;
	RECT rItem;
};

