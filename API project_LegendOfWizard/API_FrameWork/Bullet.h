#pragma once

#ifndef __BULLET_H__
#define __BULLET_H__


#include "Obj.h"
class CBullet : public CObj
{
public:
	enum STATE {FIRST, SECOND, THIRD, FOURTH, END};

public:
	CBullet();
	virtual ~CBullet();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	void Set_State();
	void Update_Frame();

	void Update_HitBox() override;

public:
	void Set_Dir(BULLET::DIR _eDir) { m_eDir = _eDir; }

private:
	BULLET::DIR		m_eDir;
	float playerFX;
	float playerFY;
	float			fDistanceBetweenSlashAndPlayerX;
	float			fDistanceBetweenSlashAndPlayerY;
	float fDistanceForHitBoxX;
	float fDistanceForHitBoxY;
	STATE m_eCurState;
	STATE m_ePreState;
};


#endif // !__BULLET_H__
