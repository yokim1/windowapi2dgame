#include "stdafx.h"
#include "Bullet.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "ObjMgr.h"
#include "SoundMgr.h"

CBullet::CBullet()
	: m_eDir(BULLET::END), m_eCurState(END), m_ePreState(END)
{

}

CBullet::~CBullet()
{
	Release();
}

void CBullet::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/Player/WOL_NORMAL_ATTACK.bmp", L"NORMAL_ATTACK");

	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = 5.f;

	m_fDis = 100.f;

	m_eCurState = FIRST;

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	playerFX = m_pTarget->Get_INFO().fX;
	playerFY = m_pTarget->Get_INFO().fY;

	
}

int CBullet::Update()
{
	if (m_bDead)
		return OBJ_DEAD;
	
	//m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	
//	m_pTarget;
	m_tInfo.fX = playerFX + cosf(m_fAngle * PI / 180.f); //* m_fSpeed;
	m_tInfo.fY = playerFY - sinf(m_fAngle * PI / 180.f);//- 20.f; //* m_fSpeed;

	//m_tInfo.fX = m_pTarget->Get_INFO().fX + cosf(m_fAngle * PI / 180.f); //* m_fSpeed;
	//m_tInfo.fY = m_pTarget->Get_INFO().fY - sinf(m_fAngle * PI / 180.f);//- 20.f; //* m_fSpeed;

	Set_State();

	Update_HitBox();
	Update_Frame();
	Update_Rect();

	return OBJ_NOEVENT;
}

void CBullet::Late_Update()
{
	/*if (100 >= m_tRect.left || 100 >= m_tRect.top
		|| WINCX - 100 <= m_tRect.right || WINCY - 100 <= m_tRect.bottom)
		m_bDead = true;*/

	//if (0 >= m_tRect.left || 0 >= m_tRect.top
	//	|| WINCX <= m_tRect.right || WINCY <= m_tRect.bottom)
	//	m_bDead = true;

	//Set_State();
}

void CBullet::Render(HDC _DC)
{
	Update_Rect();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"NORMAL_ATTACK");

	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, playerFX + iScrollX + fDistanceBetweenSlashAndPlayerX
		, playerFY + iScrollY + fDistanceBetweenSlashAndPlayerY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 200, m_tFrame.iStateY * 200
		, 200, 200
		, RGB(255, 0, 255));

	CSoundMgr::Get_Instance()->PlaySound(L"NORMAL_ATTACK_1.mp3", CSoundMgr::PLAYER);
}

void CBullet::Release()
{
}

void CBullet::Set_State() {

	if (m_ePreState != m_eCurState)
	{
		if ((-22.5f < m_fAngle && m_fAngle <= 0.f) || (0.f <= m_fAngle && m_fAngle < 22.5)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::RIGHT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = 0;
			fDistanceBetweenSlashAndPlayerY = -50;
			fDistanceForHitBoxX = 40;
			fDistanceForHitBoxY = 0;
		}
		else if (22.5f <= m_fAngle && m_fAngle <= 67.5) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::RIGHTUP;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount(); 
			fDistanceBetweenSlashAndPlayerX = -10;
			fDistanceBetweenSlashAndPlayerY = -80;
			fDistanceForHitBoxX = 30;
			fDistanceForHitBoxY = -25;
		}
		else if (67.5f <= m_fAngle && m_fAngle <= 112.5) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::UP;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -50;
			fDistanceBetweenSlashAndPlayerY = -90;
			fDistanceForHitBoxX = 0;
			fDistanceForHitBoxY = -25;
		}
		else if (112.5f <= m_fAngle && m_fAngle <= 157.5) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::LEFTUP;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -80;
			fDistanceBetweenSlashAndPlayerY = -80;
			fDistanceForHitBoxX = -25;
			fDistanceForHitBoxY = -25;
		}
		else if ((157.5f <= m_fAngle && m_fAngle <= 180.f) || (-180.f <= m_fAngle && m_fAngle <= -157.5f)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::LEFT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -100;
			fDistanceBetweenSlashAndPlayerY = -50;
			fDistanceForHitBoxX = -25;
			fDistanceForHitBoxY = 0;
		}
		else if (-157.5f <= m_fAngle && m_fAngle <= -112.5) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::LEFTDOWN;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -70;
			fDistanceBetweenSlashAndPlayerY = -5;
			fDistanceForHitBoxX = -20;
			fDistanceForHitBoxY = 30;
		}
		else if (-112.5f <= m_fAngle && m_fAngle <= -67.5) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::DOWN;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -50;
			fDistanceBetweenSlashAndPlayerY = 0;
			fDistanceForHitBoxX = 0;
			fDistanceForHitBoxY = 50;
		}
		else if (-67.5f <= m_fAngle && m_fAngle <= -22.5) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = BULLET::RIGHTDOWN;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -5;
			fDistanceForHitBoxX = 25;
			fDistanceForHitBoxY = 35;
		}
		m_ePreState = m_eCurState;
	}
}

void CBullet::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			m_eCurState = FIRST;
			m_bDead = true;

			m_tFrame.iStartX = 0;
		}
	}
}

void CBullet::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX + fDistanceForHitBoxX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY + fDistanceForHitBoxY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX + fDistanceForHitBoxX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY + fDistanceForHitBoxY;
}