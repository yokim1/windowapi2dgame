#include "stdafx.h"
#include "Guide_Bullet.h"
#include "ObjMgr.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "CollisionMgr.h"

CGuide_Bullet::CGuide_Bullet()
{
}


CGuide_Bullet::~CGuide_Bullet()
{
}

void CGuide_Bullet::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ICE_BLAST.bmp", L"ICE_BLAST");
	
	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = 5.f;

	m_tFrame.iStartX = 0;
	m_tFrame.iEndX = 3;

	m_bIsHit = false;
}

int CGuide_Bullet::Update()
{
	if (m_bDead)
		return OBJ_DEAD;

	m_pTarget = CObjMgr::Get_Instance()->Get_Target(this, OBJID::MONSTER);
	
	if (m_bIsHit==false)
	{
		//m_pTarget = CObjMgr::Get_Instance()->Get_SwordMan();

		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;

		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		float x = 0.f;
		float y = 0.f;
		
		/*if (CCollisionMgr::Check_Rect(m_pTarget, this, &x, &y)) {
			m_bIsHit = true;
		}*/

		/*if (CCollisionMgr::HitBoxCollision(m_pTarget, this)) {
			m_bIsHit = true;
		}*/
	}
	else {
		m_pTarget = CObjMgr::Get_Instance()->Get_Player();

		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		/*if (CCollisionMgr::HitBoxCollision(m_pTarget, this)) {
			Set_Dead();
		}*/
	}

	m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
	m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

	Update_Rect();
	Update_Frame();

	return OBJ_NOEVENT;
}

void CGuide_Bullet::Late_Update()
{
	/*if (100 >= m_tRect.left || 100 >= m_tRect.top
		|| WINCX - 100 <= m_tRect.right || WINCY - 100 <= m_tRect.bottom)
		m_bDead = true;*/
}

void CGuide_Bullet::Render(HDC _DC)
{
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"ICE_BLAST");
	
	float fScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	float fScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();
	
	GdiTransparentBlt(_DC
		, m_tRect.left + fScrollX, m_tRect.top + fScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 200, m_tFrame.iStateY * 250
		, 200, 250
		, RGB(255, 0, 255));

	//Ellipse(_DC, m_tRect.left + fScrollX, m_tRect.top + fScrollY, m_tRect.right + fScrollX, m_tRect.bottom + fScrollY);

}

void CGuide_Bullet::Release()
{
}

void CGuide_Bullet::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
}

void CGuide_Bullet::Update_Frame()
{
	if (m_tFrame.dwTime + 200 < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			m_tFrame.iStartX = 0;
		}
	}
}