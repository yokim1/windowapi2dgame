#include "stdafx.h"
#include "CPlayerBar.h"
#include "BmpMgr.h"

CPlayerBar::CPlayerBar()
{
}


CPlayerBar::~CPlayerBar()
{
}

void CPlayerBar::Initialize() {
	m_eRenderID = RENDERID::UI;

	m_tInfo.iCX = 174;//328;
	m_tInfo.iCY = 50;//80;
}

int CPlayerBar::Update() {
	if (m_bDead) {
		return OBJ_DEAD;
	}

	Update_Rect();

	return OBJ_NOEVENT;
}

void CPlayerBar::Late_Update() {

}

void CPlayerBar::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_PLAYERBAR");

	GdiTransparentBlt(_DC
		, 50, 50
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, 0, 0
		, 328, 80
		, RGB(255, 0, 255));
}

void CPlayerBar::Release() {

}