#include "stdafx.h"
#include "CArcherAttack.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "ObjMgr.h"
#include "SoundMgr.h"


CArcherAttack::CArcherAttack()
	: m_eDir(BULLET::END)//, m_eCurState(END), m_ePreState(END)
{
}


CArcherAttack::~CArcherAttack()
{
	Release();
}

void CArcherAttack::Initialize()
{
	m_eRenderID = RENDERID::EFFECT;

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ARCHER_BOW_LEFT.bmp", L"LEFT_ARROW");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ARCHER_BOW_RIGHT.bmp", L"RIGHT_ARROW");
	
	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_fSpeed = 20.f;

	m_fDis = 100.f;

	m_tFrame.iStartX = 4;


	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	if (m_tInfo.fX < m_pTarget->Get_INFO().fX)
		m_pFrameKey = L"RIGHT_ARROW";
	else
		m_pFrameKey = L"LEFT_ARROW";

	m_bShot = true;

	
}

int CArcherAttack::Update()
{
	if (1* TILECX >= m_tRect.left || 1*TILECY >= m_tRect.top
		|| 25*TILECX <= m_tRect.right || 31*TILECY <= m_tRect.bottom)
		m_bDead = true;

	if (m_bDead)
		return OBJ_DEAD;

	m_tInfo.fX = m_tInfo.fX + cosf(m_fAngle * PI / 180.f) * m_fSpeed;
	m_tInfo.fY = m_tInfo.fY - sinf(m_fAngle * PI / 180.f) * m_fSpeed;

	if (m_bShot) {
		if (m_tInfo.fX < m_pTarget->Get_INFO().fX)
			m_pFrameKey = L"RIGHT_ARROW";
		else
			m_pFrameKey = L"LEFT_ARROW";
		m_bShot = false;
	}

	Update_Frame();
	Update_Rect();

	return OBJ_NOEVENT;
}

void CArcherAttack::Late_Update()
{
	//���� ���
	if ((78.75 <= m_fAngle && m_fAngle <= 90) ||
		(90 <= m_fAngle && m_fAngle <= 101.25)) {
		m_tFrame.iStateY = UP;
	}
	else if ((56.25 <= m_fAngle && m_fAngle <= 78.75) ||
		(101.25 <= m_fAngle && m_fAngle <= 123.75)) {
		m_tFrame.iStateY = UP675;
	}
	else if (33.75 <= m_fAngle && m_fAngle <= 56.25 ||
		(123.75 <= m_fAngle && m_fAngle <= 146.25)) {
		m_tFrame.iStateY = UP45;
	}
	else if (11.25 <= m_fAngle && m_fAngle <= 33.75 ||
		(146.25 <= m_fAngle && m_fAngle <= 168.75)) {
		m_tFrame.iStateY = UP225;
	}
	else if (0 <= m_fAngle && m_fAngle <= 11.25 ||
		(168.75 <= m_fAngle && m_fAngle <= 180)) {
		m_tFrame.iStateY = SIDE;
	}
	//�Ʒ��� ���
	else if (-180 <= m_fAngle && m_fAngle <= -168.75 ||
		(-11.25 <= m_fAngle && m_fAngle <= 0)) {
		//Left || right
		m_tFrame.iStateY = SIDE;
	}
	else if (-168.75 <= m_fAngle && m_fAngle <= -146.25 ||
		(-33.75 <= m_fAngle && m_fAngle <= 11.25)) {
		//right Down 157.5
		m_tFrame.iStateY = DOWN225;
	}
	else if (-146.25 <= m_fAngle && m_fAngle <= -123.75 ||
		(-56.25 <= m_fAngle && m_fAngle <= -33.75)) {
		//right Down 135
		m_tFrame.iStateY = DOWN45;
	}
	else if (-123.75 <= m_fAngle && m_fAngle <= -101.25 ||
		(-78.75 <= m_fAngle && m_fAngle <= -56.25)) {
		//right Down 112.5
		m_tFrame.iStateY = DOWN675;
	}
	else if (-101.25 <= m_fAngle && m_fAngle <= -90 ||
		(-90 <= m_fAngle && m_fAngle <= -78.75)) {
		//Down
		m_tFrame.iStateY = DOWN;
	}

	Set_State();
}

void CArcherAttack::Render(HDC _DC)
{
	Update_Rect();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tInfo.fX + iScrollX - 50
		, m_tInfo.fY + iScrollY - 50
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 180, m_tFrame.iStateY * 170
		, 180, 170
		, RGB(0, 255, 255));
}

void CArcherAttack::Release()
{

}

void CArcherAttack::Set_State() {

	/*if (m_ePreState != m_eCurState)
	{
		if (( -11.25 <= m_fAngle && m_fAngle <= 0)|| (0 <= m_fAngle && m_fAngle <= 11.25)) {
			m_tFrame.iStartX = 4;
			m_tFrame.iEndX = 5;
			m_tFrame.iStateY = 4;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -50;
		}
		else if ((-33.75 <= m_fAngle && m_fAngle <= -11.25)) {
			m_tFrame.iStartX = 4;
			m_tFrame.iEndX = 5;
			m_tFrame.iStateY = 5;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -50;
		}
		else if ((-67.25 <= m_fAngle && m_fAngle <= -33.75)) {
			m_tFrame.iStartX = 4;
			m_tFrame.iEndX = 5;
			m_tFrame.iStateY = 6;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -50;
		}
		else if ((-78.75 <= m_fAngle && m_fAngle <= -67.25)) {
			m_tFrame.iStartX = 4;
			m_tFrame.iEndX = 5;
			m_tFrame.iStateY = 7;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -50;
		}

		m_ePreState = m_eCurState;
	}*/
}

void CArcherAttack::Update_Frame()
{
	//if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	//{
	//	++m_tFrame.iStartX;
	//	m_tFrame.dwTime = GetTickCount();

	//	if (m_tFrame.iStartX >= m_tFrame.iEndX)
	//	{
	//		//m_bDead = true;
	//		m_tFrame.iStartX = 4;
	//	}
	//}
}

void CArcherAttack::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY;
}