#pragma once
#include "Obj.h"
class CCoin :
	public CObj
{
public:
	CCoin();
	virtual ~CCoin();

public:
	virtual void Initialize();
	virtual int Update();
	virtual void Late_Update();
	virtual void Render(HDC _DC) ;
	virtual void RenderHitBox(HDC _DC);
	virtual void Release() ;

	void Update_Frame();

	int value;
};

