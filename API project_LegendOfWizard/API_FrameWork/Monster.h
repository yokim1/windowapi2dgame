#pragma once

#ifndef __MONSTER_H__
#define __MONSTER_H__


#include "Obj.h"
class CMonster : public CObj
{
public:
	enum STATE { IDLE, WALK, ATTACK, HIT, DEAD, END };

public:
	CMonster();
	virtual ~CMonster();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	POINT		m_tPosin;

	void Update_Frame();
	void Set_State();

	void Update_Range();
	void Key_State();

	void Update_HitBox() override;

private:
	template <typename T>
	CObj* Create_Bullet(float fX, float fY)
	{
		CObj* pObj = CAbstractFactory<T>::Create(fX, fY, m_fAngle);

		return pObj;
	}

private:
	//float	m_fRadius;
	RECT	m_tRangeRect;

	STATE	m_eCurState;
	STATE	m_ePreState;
};


#endif // !__MONSTER_H__
