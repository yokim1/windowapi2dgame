#pragma once
#include "Obj.h"
class CWizardFire : public CObj
{
public:
	enum STATE {
		HIT_IDLE,
		RIGHT, RIGHTDOWN225, RIGHTDOWN45, RIGHTDOWN675, DOWN, LEFTDOWN1125, LEFTDOWN135, LEFTDOWN1575,
		LEFT, LEFTUP1575, LEFTUP135, LEFTUP1125, UP, RIGHTUP675, RIGHTUP45, RIGHTUP225,
		END
	};

public:
	CWizardFire();
	virtual ~CWizardFire();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	void Set_State();
	void Update_Frame();
	void Update_Direction();

	void Update_HitBox() override;

public:
	void Set_Dir(BULLET::DIR _eDir) { m_eDir = _eDir; }

private:
	BULLET::DIR		m_eDir;

	float			playerFX;
	float			playerFY;

	float			fDistanceBetweenSlashAndPlayerX;
	float			fDistanceBetweenSlashAndPlayerY;

	STATE m_eCurState;
	STATE m_ePreState;
};

