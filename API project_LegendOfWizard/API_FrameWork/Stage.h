#pragma once

#ifndef __STAGE_H__
#define __STAGE_H__


#include "Scene.h"
class CStage : public CScene
{
public:
	CStage();
	virtual ~CStage();

public:
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	RECT m_tBossRecognizeArea;
	bool m_IsBossCreated;

	RECT m_tArcherRecognizeArea;
	bool m_IsArcherCreated;


	bool m_bBegin;

	int m_iNumberOfEnemies;
};

#endif // !__STAGE_H__
