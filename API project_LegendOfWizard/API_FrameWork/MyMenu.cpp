#include "stdafx.h"
#include "MyMenu.h"
#include "MyButton.h"
#include "ObjMgr.h"
#include "BmpMgr.h"
#include "KeyMgr.h"
#include "SceneMgr.h"
#include "Mouse.h"
#include "SoundMgr.h"


CMyMenu::CMyMenu()
	: m_eChoice(MAIN)
{
}


CMyMenu::~CMyMenu()
{
	Release();
}

void CMyMenu::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MAIN_MENU.bmp", L"MAIN");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/OPTION_MENU.bmp", L"OPTION");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/QUIT_MENU.bmp", L"QUIT");

	CObj* pObj = CAbstractFactory<CMouse>::Create();
	CObjMgr::Get_Instance()->Add_Object(OBJID::MOUSE, pObj);
}

void CMyMenu::Update()
{
	CObjMgr::Get_Instance()->Update();
}

void CMyMenu::Late_Update()
{
	CSoundMgr::Get_Instance()->PlaySound(L"MAIN_MENU_BGM.mp3", CSoundMgr::BGM);
	if (CKeyMgr::Get_Instance()->Key_Down(VK_UP) && (0 < m_eChoice))
	{
		m_eChoice = m_eChoice - 1;
		CSoundMgr::Get_Instance()->PlaySound(L"SELECT_MENU.mp3", CSoundMgr::BGM);
		return;
	}

	if (CKeyMgr::Get_Instance()->Key_Down(VK_DOWN) && (m_eChoice < END - 1))
	{
		m_eChoice = m_eChoice + 1;
		CSoundMgr::Get_Instance()->PlaySound(L"SELECT_MENU.mp3", CSoundMgr::BGM);
		return;
	}

	POINT	pt = {};
	GetCursorPos(&pt);
	ScreenToClient(g_hWnd, &pt);

	if (PtInRect(&m_tMenuRectMAIN, pt)) {
		CSoundMgr::Get_Instance()->PlaySound(L"SELECT_MENU.mp3", CSoundMgr::BGM);
		m_eChoice = MAIN;
	}
	else if (PtInRect(&m_tMenuRectOPTION, pt)) {
		CSoundMgr::Get_Instance()->PlaySound(L"SELECT_MENU.mp3", CSoundMgr::BGM);
		m_eChoice = OPTION;
	}
	else if (PtInRect(&m_tMenuRectQUIT, pt)) {
		CSoundMgr::Get_Instance()->PlaySound(L"SELECT_MENU.mp3", CSoundMgr::BGM);
		m_eChoice = QUIT;		
	}

	if ((CKeyMgr::Get_Instance()->Key_Down(VK_RETURN)) ||
		(CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON) && PtInRect(&m_tMenuRectMAIN, pt)) ||
		(CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON) && PtInRect(&m_tMenuRectOPTION, pt)) ||
		(CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON) && PtInRect(&m_tMenuRectQUIT, pt)))
	{
		
		switch (m_eChoice)
		{
		case CMyMenu::MAIN:
			CSceneMgr::Get_Instance()->Scene_Change(CSceneMgr::STAGE);
			return;
			break;

		case CMyMenu::OPTION:
			CSceneMgr::Get_Instance()->Scene_Change(CSceneMgr::EDIT);
			return;
			break;

		case CMyMenu::QUIT:
			exit(1);
			break;

		default:
			break;
		}
	}

	CObjMgr::Get_Instance()->Late_Update();
}

void CMyMenu::Render(HDC _DC)
{
	HDC hMemDC = _DC; //= CBmpMgr::Get_Instance()->Find_Bmp(L"MAIN");

	switch (m_eChoice)
	{
	case CMyMenu::MAIN:
		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"MAIN");
		break;
	
	case CMyMenu::OPTION:
		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"OPTION");
		break;
	
	case CMyMenu::QUIT:
		hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"QUIT");
		break;

	case CMyMenu::END:
		break;

	default:
		break;
	}

	StretchBlt(_DC, 0, 0, WINCX, WINCY, hMemDC, 0, 0, 1920, 1080, SRCCOPY);

	CObjMgr::Get_Instance()->Render(_DC);
}

void CMyMenu::Release()
{

}
