#include "stdafx.h"
#include "CArcher.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CArcherAttack.h"
#include "CBow.h"
#include "CCoin.h"
#include "SoundMgr.h"
#include "CCountMgr.h"

CArcher::CArcher()
{
}


CArcher::~CArcher()
{
}

void CArcher::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ARCHER_LEFT.bmp", L"ARCHER_LEFT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ARCHER_RIGHT.bmp", L"ARCHER_RIGHT");

	m_tInfo.iCX = 100;
	m_tInfo.iCY = 100;

	m_tInfo.iHP = 500;

	m_fSpeed = 3.f;

	m_eRenderID = RENDERID::OBJECT;

	m_pFrameKey = L"ARCHER_LEFT";

	pObj = nullptr;

	CCountMgr::Get_Instance()->IncreaseCount();

}

int CArcher::Update() {

	if (m_bDead) {
		CObj* pObj = CAbstractFactory<CCoin>::Create(m_tInfo.fX, m_tInfo.fY);
		CObjMgr::Get_Instance()->Add_Object(OBJID::COIN, pObj);
		CSoundMgr::Get_Instance()->PlaySound(L"ENEMY_DIED.mp3", CSoundMgr::MONSTER);
		CCountMgr::Get_Instance()->DecreaseCount();

		return OBJ_DEAD;
	}

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };
	RECT rcTemp;
	
	

	//bCheck_Rect의 parameter를 충돌 Rect들로 바꿔야한다.
	//만약
	//target이 range안에 있다면 쫓아가라
	if (m_tInfo.iHP <= 0) {
		m_eCurState = DEAD;
	}
	else if (m_bHit) {
		m_pTarget->IncreaseMp();
		m_eCurState = HIT;
		m_tInfo.iHP -= 5;
		m_bHit = false;
		CSoundMgr::Get_Instance()->PlaySound(L"HIT_SOUND_NORMAL_1.mp3", CSoundMgr::EFFECT);
	}
	else if (IntersectRect(&rcTemp, &m_tAttackRangeRect, &m_pTarget->Get_Rect()))
	{
		m_eCurState = ATTACK;
		//m_bShot = true;
	}
	else if (PtInRect(&m_tRangeRect, pt) && m_eCurState != ATTACK)//!CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		//	fRad = 2 * PI - fRad;
		//m_fAngle = fRad * 180.f / PI;

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
		m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

		if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
			m_pFrameKey = L"ARCHER_RIGHT";
		else
			m_pFrameKey = L"ARCHER_LEFT";

		m_eCurState = WALK;
	}
	else
	{
		m_eCurState = IDLE;
	}

	
	Set_State();

	Update_Rect();
	Update_Range();

	Update_Frame();

	Key_State();

	return OBJ_NOEVENT;
}

void CArcher::Late_Update() {

}

void CArcher::Render(HDC _DC) {
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	//Range
	//Ellipse(_DC, m_tRangeRect.left + iScrollX, m_tRangeRect.top + iScrollY, m_tRangeRect.right + iScrollX, m_tRangeRect.bottom + iScrollY);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 200, m_tFrame.iStateY * 200
		, 200, 200
		, RGB(255, 0, 255));
}

void CArcher::Release() {

}

void CArcher::Update_Frame() {
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			if (m_eCurState == DEAD) {
				Set_Dead();
			}

			/*if (m_eCurState == ATTACK || m_eCurState == WALK) {
				m_eCurState = IDLE;
				m_bShot = false;
			}*/

			/*if (m_eCurState == ATTACK) {
				CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CArcherAttack>());
			}*/

			m_tFrame.iStartX = 0;
		}
	}
}

void CArcher::Set_State() {
	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState)
		{
		case CArcher::IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 0;
			m_tFrame.iStateY = IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();

			if (pObj != nullptr) {
				pObj->Set_Dead();
				pObj = nullptr;
			}
			break;

		case CArcher::WALK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 5;
			m_tFrame.iStateY = WALK;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();

			if (pObj != nullptr) {
				pObj->Set_Dead();
				pObj = nullptr;
			}
			break;

		case CArcher::ATTACK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = ATTACK;
			m_tFrame.dwSpeed = 10000;
			m_tFrame.dwTime = GetTickCount();

			pObj = CAbstractFactory<CBow>::Create(m_tInfo.fX, m_tInfo.fY);
			CObjMgr::Get_Instance()->Add_Object(OBJID::UI, pObj);
			CSoundMgr::Get_Instance()->PlaySound(L"ARCHER_AIM.mp3", CSoundMgr::MONSTER);
			break;

		case CArcher::HIT:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = HIT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();

			if (pObj != nullptr) {
				pObj->Set_Dead();
				pObj = nullptr;
			}
			break;

		case CArcher::DEAD:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 6;
			m_tFrame.iStateY = DEAD;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();

			if (pObj != nullptr) {
				pObj->Set_Dead();
				pObj = nullptr;
			}
			break;
		}
		m_ePreState = m_eCurState;
	}
}

void CArcher::Update_Range() {
	m_tRangeRect.left = (LONG)(m_tInfo.fX - (600 >> 1));
	m_tRangeRect.top = (LONG)(m_tInfo.fY - (600 >> 1));
	m_tRangeRect.right = (LONG)(m_tInfo.fX + (600 >> 1));
	m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (600 >> 1));

	m_tAttackRangeRect.left = (LONG)(m_tInfo.fX - (300 >> 1));
	m_tAttackRangeRect.top = (LONG)(m_tInfo.fY - (300 >> 1));
	m_tAttackRangeRect.right = (LONG)(m_tInfo.fX + (300 >> 1));
	m_tAttackRangeRect.bottom = (LONG)(m_tInfo.fY + (300 >> 1));
}

void CArcher::Key_State() 
{
	/*if (m_eCurState == ATTACK && m_tFrame.iStartX == 1) {
		if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CArcherAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if ((45 <= m_fAngle && m_fAngle < 135)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CArcherAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CArcherAttack>(m_tInfo.fX, m_tInfo.fY));
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			CObjMgr::Get_Instance()->Add_Object(OBJID::BULLET, Create_Bullet<CArcherAttack>(m_tInfo.fX, m_tInfo.fY));
		}
	}*/

	/*if (m_eCurState == ATTACK && m_tFrame.iStartX == 4 && m_bAttack == false) {
		CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CArcherAttack>());
		m_bShot = true;
	}*/
}

void CArcher::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 3)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 3)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY;
}