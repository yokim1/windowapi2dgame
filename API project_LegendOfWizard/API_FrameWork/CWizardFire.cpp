#include "stdafx.h"
#include "CWizardFire.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "ObjMgr.h"

CWizardFire::CWizardFire()
	: m_eDir(BULLET::END), m_eCurState(END), m_ePreState(END)
{

}


CWizardFire::~CWizardFire()
{
	Release();
}

void CWizardFire::Initialize()
{
	m_eRenderID = RENDERID::EFFECT;

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WIZARD_FIRE.bmp", L"WIZARD_FIRE");

	m_tInfo.iCX = 50;
	m_tInfo.iCY = 50;

	m_fSpeed = 5.f;

	m_fDis = 100.f;

	m_eCurState = END;

	//m_pTarget = CObjMgr::Get_Instance()->Get_SwordMan();
	//playerFX = m_tInfo.fX;
	//playerFY = m_tInfo.fX;
}

int CWizardFire::Update()
{
	/*if (m_bDead && m_tFrame.iStartX >= m_tFrame.iEndX)
		return OBJ_DEAD;*/

	if (1 * TILECX >= m_tInfo.fX || 1 * TILECY >= m_tInfo.fY
		|| 25 * TILECX <= (m_tInfo.fX + 50) || 31 * TILECY <= (m_tInfo.fY + 50))
		m_bDead = true;

	if (m_bDead) {
		return OBJ_DEAD;
	}

	m_tInfo.fX = m_tInfo.fX + cosf(m_fAngle * PI / 180.f)* m_fSpeed;
	m_tInfo.fY = m_tInfo.fY - sinf(m_fAngle * PI / 180.f) * m_fSpeed;//- 20.f; //* m_fSpeed;

	//Set_State();
	Update_Direction();
	Update_Frame();
	Update_Rect();
	Update_HitBox();

	return OBJ_NOEVENT;
}

void CWizardFire::Late_Update()
{
	//Set_State();
}

void CWizardFire::Render(HDC _DC)
{
	Update_Rect();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"WIZARD_FIRE");

	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tInfo.fX + iScrollX //+ fDistanceBetweenSlashAndPlayerX
		, m_tInfo.fY + iScrollY //+ fDistanceBetweenSlashAndPlayerY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 90, m_tFrame.iStateY * 90
		, 90, 90
		, RGB(110, 70, 210));

//	Rectangle(_DC, m_tInfo.fX, m_tInfo.fY, m_tInfo.fX + 100, m_tInfo.fY + 100);
}

void CWizardFire::Release()
{
}

void CWizardFire::Set_State() {

	if (m_ePreState != m_eCurState)
	{
		if ((-45 < m_fAngle && m_fAngle <= 0) || (0 <= m_fAngle && m_fAngle < 45)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			//m_tFrame.iStateY = RIGHT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -20;
			fDistanceBetweenSlashAndPlayerY = -50;
		}
		else if ((45 <= m_fAngle && m_fAngle < 135)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			//m_tFrame.iStateY = UP;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -50;
			fDistanceBetweenSlashAndPlayerY = -80;
		}
		else if ((135 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle < -135)) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			//m_tFrame.iStateY = LEFT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -90;
			fDistanceBetweenSlashAndPlayerY = -50;
		}
		else if (-135 <= m_fAngle && m_fAngle <= -45) {
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			//m_tFrame.iStateY = DOWN;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			fDistanceBetweenSlashAndPlayerX = -50;
			fDistanceBetweenSlashAndPlayerY = 10;
		}

		m_ePreState = m_eCurState;
	}
}

void CWizardFire::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			//m_bDead = true;
			m_tFrame.iStartX = 0;
		}
	}
}

void CWizardFire::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX + 25; //+ fDistanceBetweenSlashAndPlayerX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 2)) + iScrollY + 25; //+ fDistanceBetweenSlashAndPlayerY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX + 25; //+ fDistanceBetweenSlashAndPlayerX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 2)) + iScrollY + 25; //+ fDistanceBetweenSlashAndPlayerY;
}

void CWizardFire::Update_Direction() {
	if (m_ePreState != m_eCurState)
	{
	if ((0 <= m_fAngle && m_fAngle <= 11.25)) {
		m_tFrame.iStateY = RIGHT;
		m_eCurState = RIGHT;
	}
	else if (11.25 <= m_fAngle && m_fAngle <= 33.75) {
		m_tFrame.iStateY = RIGHTUP225;
		m_eCurState = RIGHTUP225;
	}
	else if (33.75 <= m_fAngle && m_fAngle <= 56.25) {
		m_tFrame.iStateY = RIGHTUP45;
		m_eCurState = RIGHTUP45;
	}
	else if (56.25 <= m_fAngle && m_fAngle <= 78.75) {
		m_tFrame.iStateY = RIGHTUP675;
		m_eCurState = RIGHTUP675;
	}
	else if (78.75 <= m_fAngle && m_fAngle <= 101.25) {
		m_tFrame.iStateY = UP;
		m_eCurState = UP;
	}
	else if (101.25 <= m_fAngle && m_fAngle <= 123.75) {
		m_tFrame.iStateY = LEFTUP1125;
		m_eCurState = LEFTUP1125;
	}
	else if (123.75 <= m_fAngle && m_fAngle <= 146.25) {
		m_tFrame.iStateY = LEFTUP135;
		m_eCurState = LEFTUP135;
	}
	else if (146.25 <= m_fAngle && m_fAngle <= 168.75) {
		m_tFrame.iStateY = LEFTUP1575;
		m_eCurState = LEFTUP1575;
	}
	else if ((168.75 <= m_fAngle && m_fAngle <= 180) || (-180 <= m_fAngle && m_fAngle <= -168.75)) {
		m_tFrame.iStateY = LEFT;
		m_eCurState = LEFT;
	}
	else if (-168.75 <= m_fAngle && m_fAngle <= -146.25) {
		m_tFrame.iStateY = LEFTDOWN1575;
		m_eCurState = LEFTDOWN1575;
	}
	else if (-146.25 <= m_fAngle && m_fAngle <= -123.75) {
		m_tFrame.iStateY = LEFTDOWN135;
		m_eCurState = LEFTDOWN135;
	}
	else if (-123.75 <= m_fAngle && m_fAngle <= -101.25) {
		m_tFrame.iStateY = LEFTDOWN1125;
		m_eCurState = LEFTDOWN1125;
	}
	else if (-101.25 <= m_fAngle && m_fAngle <= -78.75) {
		m_tFrame.iStateY = DOWN;
		m_eCurState = DOWN;
	}
	else if (-78.75 <= m_fAngle && m_fAngle <= -56.25) {
		m_tFrame.iStateY = RIGHTDOWN675;
		m_eCurState = RIGHTDOWN675;
	}
	else if (-56.25 <= m_fAngle && m_fAngle <= -33.75) {
		m_tFrame.iStateY = RIGHTDOWN45;
		m_eCurState = RIGHTDOWN45;
	}
	else if (-33.75 <= m_fAngle && m_fAngle <= -11.25) {
		m_tFrame.iStateY = RIGHTDOWN225;
		m_eCurState = RIGHTDOWN225;
	}
	else if ((-11.25 <= m_fAngle && m_fAngle <= 0)) {
		m_tFrame.iStateY = RIGHT;
		m_eCurState = RIGHT;
	}
	m_tFrame.iStartX = 0;
	m_tFrame.iEndX = 4;
	//m_tFrame.iStateY = RIGHT;
	m_tFrame.dwSpeed = 100;
	m_tFrame.dwTime = GetTickCount();
	//fDistanceBetweenSlashAndPlayerX = -20;
	//fDistanceBetweenSlashAndPlayerY = -50;

	m_ePreState = m_eCurState;
	}
}