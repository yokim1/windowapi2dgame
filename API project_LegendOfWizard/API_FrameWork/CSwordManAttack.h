#pragma once
#include "Obj.h"

class CSwordManAttack : public CObj
{

public:
	enum STATE { LEFT, RIGHT, UP, SECONDUP, DOWN, SECONDDOWN, END };

public:
	CSwordManAttack();
	~CSwordManAttack();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	void Set_State();
	void Update_Frame();

	void Update_HitBox() override;

public:
	void Set_Dir(BULLET::DIR _eDir) { m_eDir = _eDir; }

private:
	BULLET::DIR		m_eDir;
	
	float			playerFX;
	float			playerFY;

	float			fDistanceBetweenSlashAndPlayerX;
	float			fDistanceBetweenSlashAndPlayerY;

	float fDistanceForHitBoxX;
	float fDistanceForHitBoxY;

	STATE m_eCurState;
	STATE m_ePreState;
};

