#include "stdafx.h"
#include "CMiddleBoss.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "ObjMgr.h"
#include "CollisionMgr.h"
#include "CWizardBall.h"
#include "CSwordManAttack.h"
#include "CMiddleBossUI.h"
#include "CWizardFire.h"
#include "CCard.h"
#include "SoundMgr.h"
#include "CCountMgr.h"

CMiddleBoss::CMiddleBoss()
{
	//ZeroMemory(&m_tPosin, sizeof(m_tPosin));
}

CMiddleBoss::~CMiddleBoss()
{
	Release();
}

void CMiddleBoss::Initialize() {

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MIDDLE_BOSS_LEFT.bmp", L"MIDDLE_BOSS_LEFT");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MIDDLE_BOSS_RIGHT.bmp", L"MIDDLE_BOSS_RIGHT");
	
	m_tInfo.iCX = 200;
	m_tInfo.iCY = 200;

	m_tInfo.iHP = 166;

	m_fSpeed = 1.f;

	m_eRenderID = RENDERID::OBJECT;

	m_pFrameKey = L"MIDDLE_BOSS_LEFT";

	CObj* pObj = CAbstractFactory<CMiddleBossUI>::Create();
	CObjMgr::Get_Instance()->Add_Object(OBJID::MIDDLEBOSSUI, pObj);
	
	CCountMgr::Get_Instance()->IncreaseCount();

 }

int CMiddleBoss::Update() {
	if (m_bDead){
		CObj* pUI = CObjMgr::Get_Instance()->Get_MiddleBossUI();
		pUI->Set_Dead();
		CSoundMgr::Get_Instance()->PlaySound(L"ENEMY_DIED_3.mp3", CSoundMgr::MONSTER);
		CCountMgr::Get_Instance()->DecreaseCount();
		return OBJ_DEAD;
	}
	RECT rcTemp;
	m_pTarget = CObjMgr::Get_Instance()->Get_Player();

	POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };

	float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
	float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
	float fDia = sqrtf(fX * fX + fY * fY);

	float fRad = acosf(fX / fDia);

	//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
	//	fRad = 2 * PI - fRad;
	//m_fAngle = fRad * 180.f / PI;

	m_fAngle = fRad * 180.f / PI;
	if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		m_fAngle *= -1.f;

	if (m_tInfo.iHP <= 0) {
		m_eCurState = DEAD;
	}
	else if (m_bHit) {
		m_pTarget->IncreaseMp();
		m_eCurState = HIT;
		m_tInfo.iHP -= 1;
		m_bHit = false;
	}
	else if (IntersectRect(&rcTemp, &m_tAttackRangeRect, &m_pTarget->Get_Rect()))
	{
		m_eCurState = ATTACK;
		//m_bShot = true;
	}
	//bCheck_Rect의 parameter를 충돌 Rect들로 바꿔야한다.
	
	// if (PtInRect(&m_tRangeRect, pt) && m_eCurState != ATTACK)
	else if (PtInRect(&m_tRangeRect, pt) && m_eCurState != ATTACK)//!CCollisionMgr::bCheck_Rect(m_pTarget, this))
	{
		float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
		float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
		float fDia = sqrtf(fX * fX + fY * fY);

		float fRad = acosf(fX / fDia);

		//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
		//	fRad = 2 * PI - fRad;
		//m_fAngle = fRad * 180.f / PI;

		m_fAngle = fRad * 180.f / PI;
		if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
			m_fAngle *= -1.f;

		m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
		m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

		if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
			m_pFrameKey = L"MIDDLE_BOSS_RIGHT";
		else
			m_pFrameKey = L"MIDDLE_BOSS_LEFT";

		m_eCurState = WALK;
	}
	else
	{
		m_eCurState = IDLE;
	}

	//else if (IntersectRect(&rcTemp, &m_tAttackRangeRect, &m_pTarget->Get_Rect()))
	//{
	//	m_eCurState = ATTACK;
	//	//m_bShot = true;
	//}
	//else if (PtInRect(&m_tRangeRect, pt) && m_eCurState != ATTACK)//!CCollisionMgr::bCheck_Rect(m_pTarget, this))
	//{
	//	float fX = m_pTarget->Get_INFO().fX - m_tInfo.fX;
	//	float fY = m_pTarget->Get_INFO().fY - m_tInfo.fY;
	//	float fDia = sqrtf(fX * fX + fY * fY);

	//	float fRad = acosf(fX / fDia);

	//	//if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
	//	//	fRad = 2 * PI - fRad;
	//	//m_fAngle = fRad * 180.f / PI;

	//	m_fAngle = fRad * 180.f / PI;
	//	if (m_tInfo.fY < m_pTarget->Get_INFO().fY)
	//		m_fAngle *= -1.f;

	//	m_tInfo.fX += cosf(m_fAngle * PI / 180.f) * m_fSpeed;
	//	m_tInfo.fY -= sinf(m_fAngle * PI / 180.f) * m_fSpeed;

	//	if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
	//		m_pFrameKey = L"MIDDLE_BOSS_RIGHT";
	//	else
	//		m_pFrameKey = L"MIDDLE_BOSS_LEFT";

	//	m_eCurState = WALK;
	//}
	//else
	//{
	//	m_eCurState = IDLE;
	//}

	if (m_tInfo.fX <= m_pTarget->Get_INFO().fX)
		m_pFrameKey = L"MIDDLE_BOSS_RIGHT";
	else
		m_pFrameKey = L"MIDDLE_BOSS_LEFT";

	Set_State();

	Update_Rect();
	Update_Range();

	Update_Frame();

	Key_State();

	return OBJ_NOEVENT;
 }

void CMiddleBoss::Late_Update() {
	//CObjMgr::Get_Instance()->Add_Object(OBJID::UI, pObj);
 }

void CMiddleBoss::Render(HDC _DC) {

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	//Range
	//Ellipse(_DC, m_tRangeRect.left + iScrollX, m_tRangeRect.top + iScrollY, m_tRangeRect.right + iScrollX, m_tRangeRect.bottom + iScrollY);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 220, m_tFrame.iStateY * 300
		, 220, 300
		, RGB(255, 0, 255));

	/*Rectangle(_DC, m_tRect.left, m_tRect.top, m_tRect.right, m_tRect.bottom);
	Rectangle(_DC, 1160, 1736, 1260,1836);*/
 }

void CMiddleBoss::Release() {

 }


void CMiddleBoss::Update_Frame() {
	
//	m_tFrame.dwTime = GetTickCount();
//	m_bSummon = true;

	if (m_eCurState == ATTACK && m_tFrame.iStartX == 1 && m_tFrame.dwTime + 100 < GetTickCount()) {
		
		if (m_bSummonSword) {
			CObj* pObj = CAbstractFactory<CCard>::Create(m_tInfo.fX - 100, m_tInfo.fY + 100, L"SUMMON_CARD_SWORDMAN");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

			pObj = CAbstractFactory<CCard>::Create(m_tInfo.fX, m_tInfo.fY + 100, L"SUMMON_CARD_SWORDMAN");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

			pObj = CAbstractFactory<CCard>::Create(m_tInfo.fX + 100, m_tInfo.fY + 100, L"SUMMON_CARD_SWORDMAN");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

			m_bSummonSword = false;

			m_tFrame.dwTime = GetTickCount();
		}
		else if (m_bSummonArcher) {

			CObj* pObj = CAbstractFactory<CCard>::Create(m_tInfo.fX - 100, m_tInfo.fY - 100, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(m_tInfo.fX, m_tInfo.fY - 100, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(m_tInfo.fX + 100, m_tInfo.fY - 100, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			m_bSummonArcher = false;

			m_tFrame.dwTime = GetTickCount();
		}
	}
}

void CMiddleBoss::Set_State() {
	if (m_ePreState != m_eCurState)
	{
		switch (m_eCurState)
		{
		case CMiddleBoss::IDLE:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 0;
			m_tFrame.iStateY = IDLE;
			m_tFrame.dwSpeed = 200;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMiddleBoss::WALK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = WALK;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMiddleBoss::ATTACK:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = ATTACK;
			m_tFrame.dwSpeed = 550;
			m_tFrame.dwTime = GetTickCount();

			break;

		case CMiddleBoss::HIT:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 2;
			m_tFrame.iStateY = HIT;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;

		case CMiddleBoss::DEAD:
			m_tFrame.iStartX = 0;
			m_tFrame.iEndX = 4;
			m_tFrame.iStateY = DEAD;
			m_tFrame.dwSpeed = 100;
			m_tFrame.dwTime = GetTickCount();
			break;
		}
		m_ePreState = m_eCurState;
	}
}

 void CMiddleBoss::Update_Range() {
	 if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	 {
		 ++m_tFrame.iStartX;
		 m_tFrame.dwTime = GetTickCount();

		 if (m_tFrame.iStartX >= m_tFrame.iEndX)
		 {
			 if (m_eCurState == DEAD) {
				 Set_Dead();
			 }

			 if (m_eCurState == ATTACK || m_eCurState == WALK ) {
				 m_eCurState = IDLE;
			 }

			 m_tFrame.iStartX = 0;
		 }
	 }
}

 void CMiddleBoss::Key_State() {
	 m_tRangeRect.left = (LONG)(m_tInfo.fX - (500 >> 1));
	 m_tRangeRect.top = (LONG)(m_tInfo.fY - (500 >> 1));
	 m_tRangeRect.right = (LONG)(m_tInfo.fX + (500 >> 1));
	 m_tRangeRect.bottom = (LONG)(m_tInfo.fY + (500 >> 1));

	 m_tAttackRangeRect.left = (LONG)(m_tInfo.fX - (1000 >> 1));
	 m_tAttackRangeRect.top = (LONG)(m_tInfo.fY - (1000 >> 1));
	 m_tAttackRangeRect.right = (LONG)(m_tInfo.fX + (1000 >> 1));
	 m_tAttackRangeRect.bottom = (LONG)(m_tInfo.fY + (1000 >> 1));

	 if (100 < m_tInfo.iHP && m_eCurState == ATTACK && m_tFrame.dwTime + 450 < GetTickCount()) {
		 CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>(m_tInfo.fX,m_tInfo.fY - 100));
		 CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>(m_tInfo.fX - 100, m_tInfo.fY - 100));
		 CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>(m_tInfo.fX + 100, m_tInfo.fY - 100));
	 }
	 else if(m_eCurState == ATTACK && m_tFrame.dwTime + 450 < GetTickCount()) {
		 CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>(m_tInfo.fX, m_tInfo.fY - 100));
		 CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>(m_tInfo.fX - 100, m_tInfo.fY - 100));
		 CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTERBULLET, Create_Bullet<CWizardFire>(m_tInfo.fX + 100, m_tInfo.fY - 100));

		 m_bSummonSword = true;
		 m_bSummonArcher = true;
	 }
}

 void CMiddleBoss::Update_HitBox() {
	 int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	 int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	 m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	 m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1)) + iScrollY;
	 m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	 m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
 }






//
//int CMiddleBoss::Update() {
//	/*if (m_bDead) {
//		CObj* pUI = CObjMgr::Get_Instance()->Get_MiddleBossUI();
//		pUI->Set_Dead();
//		return OBJ_DEAD;
//	}*/
//			
//	//RECT rcTemp;
//	//m_pTarget = CObjMgr::Get_Instance()->Get_Player();		
//	//POINT pt = { m_pTarget->Get_INFO().fX, m_pTarget->Get_INFO().fY };
//
//	/*if (m_tInfo.iHP <= 0) {
//		m_eCurState = DEAD;
//	}*/
//	/*else if (m_bHit) {
//		m_eCurState = HIT;
//		m_tInfo.iHP -= 1;
//		m_bHit = false;
//	}*/
//	//else if (IntersectRect(&rcTemp, &m_tAttackRangeRect, &m_pTarget->Get_Rect()))
//	//{
//	//	m_eCurState = ATTACK;
//	//	//m_bShot = true;
//	//}
//
//	Update_Rect();
//
//	return OBJ_NOEVENT;
//}
//
//void CMiddleBoss::Late_Update() {
//
//}
//
//void CMiddleBoss::Render(HDC _DC) {
//
//	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"MIDDLE_BOSS_LEFT");
//
//	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
//	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();
//
//		GdiTransparentBlt(_DC
//		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
//		, m_tInfo.iCX, m_tInfo.iCY
//		, hMemDC
//		, m_tFrame.iStartX * 220, m_tFrame.iStateY * 300
//		, 220, 300
//		, RGB(255, 0, 255));
//
//	//Rectangle(_DC, m_tRect.left + iScrollX, m_tRect.top + iScrollY, m_tRect.right + iScrollX, m_tRect.bottom + iScrollY);
//}
//
//void CMiddleBoss::Release() {
//
//}