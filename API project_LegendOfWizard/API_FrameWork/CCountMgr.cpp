#include "stdafx.h"
#include "CCountMgr.h"

CCountMgr* CCountMgr::m_pInstance = nullptr;

CCountMgr::CCountMgr()
{
}


CCountMgr::~CCountMgr()
{
}


void CCountMgr::Initialize() {
	m_iCount = 0;
}

void CCountMgr::Release() {

}

void CCountMgr::IncreaseCount() {
	++m_iCount;
}

void CCountMgr::DecreaseCount() {
	--m_iCount;
}
	
int	CCountMgr::GetCount() {
	return m_iCount;
}
