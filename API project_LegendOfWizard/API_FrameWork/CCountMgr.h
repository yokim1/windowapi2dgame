#pragma once
class CObj;
class CCountMgr
{
public:
	CCountMgr();
	~CCountMgr();

public:
	void Initialize();
	void Release();

	void IncreaseCount();
	void DecreaseCount();
	int	GetCount();

public:
	static CCountMgr* Get_Instance()
	{
		if (!m_pInstance)
			m_pInstance = new CCountMgr;
		return m_pInstance;
	}

	static void Destroy_Instance()
	{
		SAFE_DELETE(m_pInstance);
	}

private:
	static CCountMgr*	m_pInstance;

	int m_iCount;
};
