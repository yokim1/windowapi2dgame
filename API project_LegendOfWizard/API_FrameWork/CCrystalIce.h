#pragma once
#include "Obj.h"
class CCrystalIce :
	public CObj
{
public:
	CCrystalIce();
	virtual ~CCrystalIce();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

	void Set_State();

	void Update_Frame();

	void Update_HitBox() override;

private:
	int m_iStartTime;
	const int m_iDuration = 10;
};

