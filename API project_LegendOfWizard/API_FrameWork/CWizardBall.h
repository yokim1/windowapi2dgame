#pragma once
#include "Obj.h"
class CWizardBall :public CObj
{
public:
	enum STATE { HIT_IDLE, 
		RIGHT, RIGHTDOWN225, RIGHTDOWN45, RIGHTDOWN675, DOWN, LEFTDOWN1125, LEFTDOWN135, LEFTDOWN1575,
		LEFT, LEFTUP1575, LEFTUP135, LEFTUP1125, UP, RIGHTUP675, RIGHTUP45, RIGHTUP225, 
		END };

public:
	CWizardBall();
	virtual ~CWizardBall();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	POINT		m_tPosin;

	void Update_Frame();
	int Update_Direction();
	void Set_State();

	void Update_Range();
	void Key_State();

	void Update_HitBox() override;

private:
	RECT	m_tRangeRect;
	RECT	m_tAttackRangeRect;

	STATE	m_eCurState;
	STATE	m_ePreState;
};

