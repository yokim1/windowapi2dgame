#include "stdafx.h"
#include "CollisionMgr.h"
#include "Obj.h"

CCollisionMgr::CCollisionMgr()
{
}


CCollisionMgr::~CCollisionMgr()
{
}

void CCollisionMgr::Collision_Rect(list<CObj*>& _Dst, list<CObj*>& _Src)
{
	RECT rc = {};
	for (auto& pDst : _Dst)
	{
		for (auto& pSrc : _Src)
		{
			if (IntersectRect(&rc, &pDst->Get_Rect(), &pSrc->Get_Rect()))
			{
				pDst->Set_Dead();
				pSrc->Set_Dead();
			}
		}
	}
}

void CCollisionMgr::Collision_RectCoin(list<CObj*>& _Dst, list<CObj*>& _Src)
{
	RECT rc = {};
	for (auto& pDst : _Dst)
	{
		for (auto& pSrc : _Src)
		{
			if (IntersectRect(&rc, &pDst->Get_Rect(), &pSrc->Get_Rect()))
			{
				pDst->Set_Dead();
				//pSrc->Set_Dead();
			}
		}
	}
}

void CCollisionMgr::Collision_RectHit(list<CObj*>& _Dst, list<CObj*>& _Src)
{
	RECT rc = {};
	for (auto& pDst : _Dst)
	{
		for (auto& pSrc : _Src)
		{
			if (IntersectRect(&rc, &pDst->Get_Rect(), &pSrc->Get_Rect()))
			{
				pDst->Set_Dead();
				pSrc->Set_Hit();
			}
		}
	}
}

void CCollisionMgr::Collision_Rect_Ex(list<CObj*>& _Dst, list<CObj*>& _Src)
{
	float	fX = 0.f, fY = 0.f;

	for (auto& pDst : _Dst)
	{
		for (auto& pSrc : _Src)
		{
			if (Check_Rect(pDst, pSrc, &fX, &fY))
			{
				if (fX < fY)	// 좌우 충돌
				{
					if (pDst->Get_INFO().fX < pSrc->Get_INFO().fX)
						pSrc->Set_PosX(fX);
					else
						pSrc->Set_PosX(-fX);
				}
				else			// 상하 충돌
				{
					if (pDst->Get_INFO().fY < pSrc->Get_INFO().fY)
						pSrc->Set_PosY(fY);
					else
						pSrc->Set_PosY(-fY);
				}
			}
		}
	}
}

void CCollisionMgr::Collision_Sphere(list<CObj*>& _Dst, list<CObj*>& _Src)
{
	for (auto& pDst : _Dst)
	{
		for (auto& pSrc : _Src)
		{
			if (Check_Sphere(pDst, pSrc))
			{
				pDst->Set_Dead();
				pSrc->Set_Dead();
			}
		}
	}
}

bool CCollisionMgr::Check_Sphere(CObj* _Dst, CObj* _Src)
{
	float	fX = abs(_Dst->Get_INFO().fX - _Src->Get_INFO().fX);
	float	fY = _Dst->Get_INFO().fY - _Src->Get_INFO().fY;
	float	fDia = sqrtf(fX * fX + fY * fY);

	float	fSum = (float)((_Dst->Get_INFO().iCX + _Src->Get_INFO().iCX) >> 1);

	if (fSum >= fDia)
		return true;
	return false;
}

bool CCollisionMgr::Check_Rect(CObj* _Dst, CObj* _Src, float* _x, float* _y)
{
	float	fX = abs(_Dst->Get_INFO().fX - _Src->Get_INFO().fX);
	float	fY = abs(_Dst->Get_INFO().fY - _Src->Get_INFO().fY);

	float	fXDis = (float)((_Dst->Get_INFO().iCX + _Src->Get_INFO().iCX) >> 1);
	float	fYDis = (float)((_Dst->Get_INFO().iCY + _Src->Get_INFO().iCY) >> 1);

	if (fX < fXDis && fY < fYDis)
	{
		*_x = fXDis - fX;
		*_y = fYDis - fY;
		return true;
	}
	return false;
}

bool CCollisionMgr::bCheck_Rect(CObj* _Dst, CObj* _Src)
{
	float	fX = abs(_Dst->Get_INFO().fX - _Src->Get_INFO().fX);
	float	fY = abs(_Dst->Get_INFO().fY - _Src->Get_INFO().fY);

	float	fXDis = (float)((_Dst->Get_INFO().iCX + _Src->Get_INFO().iCX) >> 1);
	float	fYDis = (float)((_Dst->Get_INFO().iCY + _Src->Get_INFO().iCY) >> 1);

	if (fX < fXDis && fY < fYDis)
	{
		return true;
	}
	return false;
}

bool CCollisionMgr::bCheck_HitBox(CObj* _Dst, CObj* _Src) {
	RECT temp = {};

	if (IntersectRect(&temp, &_Dst->Get_HitBox(), &_Src->Get_HitBox())){
		return true;
	}
	return false;
}

void CCollisionMgr::HitBoxCollision(list<CObj*>& _Dst, list<CObj*>& _Src) {
	RECT temp = {};

	for (auto& pDst : _Dst) 
	{
		for (auto& pSrc : _Src) 
		{
			if (IntersectRect(&temp, &pDst->Get_HitBox(), &pSrc->Get_HitBox())) {
				pDst->Set_Hit();
				pSrc->Set_Hit();
			}
		}
	}
}