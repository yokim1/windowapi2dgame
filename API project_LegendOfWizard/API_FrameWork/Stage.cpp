#include "stdafx.h"
#include "Stage.h"

#include "Player.h"
#include "Monster.h"
#include "CArcher.h"
#include "CWizard.h"
#include "CWizardBall.h"
#include "CMiddleBoss.h"
#include "CBoss.h"
#include "CNpc.h"

#include "ObjMgr.h"
#include "LineMgr.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "TileMgr.h"
#include "SoundMgr.h"

#include "Mouse.h"

#include "CPlayerUI.h"
#include "CMiddleBossUI.h"
#include "CInventory.h"

#include "CPlayerBar.h"
#include "CCard.h"
#include "CCountMgr.h"


CStage::CStage()
	: m_iNumberOfEnemies(0)
{
}


CStage::~CStage()
{
	Release();
}

void CStage::Initialize()
{
	//CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/Background.bmp", L"Background");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/Tile.bmp", L"Tile");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WOL_TILE_HOMETOWN.bmp", L"WOL_TILE_HOMETOWN");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WOL_TILE_DUNGEON.bmp", L"WOL_TILE_DUNGEON");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SUMMON_CARD_SWORDMAN.bmp", L"SUMMON_CARD_SWORDMAN");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SUMMON_CARD_ARCHER.bmp", L"SUMMON_CARD_ARCHER");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SUMMON_CARD_WIZARD.bmp", L"SUMMON_CARD_WIZARD");
	

	CObj* pObj = CAbstractFactory<CPlayer>::Create(10.5f*TILECX, 5.f * TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::PLAYER, pObj);
	
	/*pObj = CAbstractFactory<CMonster>::Create(13.f * TILECX, 3.f * TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	pObj = CAbstractFactory<CMonster>::Create(13.f * TILECX, 7.f * TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	pObj = CAbstractFactory<CArcher>::Create(10.f * TILECX, 3.f * TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	pObj = CAbstractFactory<CArcher>::Create(10.f * TILECX, 7.f * TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	pObj = CAbstractFactory<CWizard>::Create(6.f * TILECX, 5.f * TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);*/

	pObj = CAbstractFactory<CCard>::Create(5.5f * TILECX, 2.5f*TILECY, L"SUMMON_CARD_SWORDMAN");
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
	
	pObj = CAbstractFactory<CCard>::Create(5.5f * TILECX, 4.5f*TILECY, L"SUMMON_CARD_SWORDMAN");
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
	
	pObj = CAbstractFactory<CCard>::Create(5.5f * TILECX, 6.5f*TILECY, L"SUMMON_CARD_SWORDMAN");
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	//pObj = CAbstractFactory<CCard>::Create(8.f * TILECX, 3.f*TILECY, L"SUMMON_CARD_ARCHER");
	//CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	//pObj = CAbstractFactory<CCard>::Create(8.f * TILECX, 7.f*TILECY, L"SUMMON_CARD_ARCHER");
	//CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

	pObj = CAbstractFactory<CCard>::Create(8.f * TILECX, 4.5f*TILECY, L"SUMMON_CARD_WIZARD");
	CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
	

	pObj = CAbstractFactory<CNpc>::Create(4.f * TILECX, 14.f*TILECY);
	CObjMgr::Get_Instance()->Add_Object(OBJID::NPC, pObj);

	pObj = CAbstractFactory<CPlayerUI>::Create();
	CObjMgr::Get_Instance()->Add_Object(OBJID::UI, pObj);

	CInventory::Get_Instance()->Initialize();

	CTileMgr::Get_Instance()->Load_Tile();

	m_tBossRecognizeArea.left = TILECX * 17.f;
	m_tBossRecognizeArea.top = TILECY * 28.f;;
	m_tBossRecognizeArea.right = TILECX * 19.f;
	m_tBossRecognizeArea.bottom = TILECY * 30.f;

	m_IsBossCreated = false;


	m_tArcherRecognizeArea.left = TILECX * 17.f;
	m_tArcherRecognizeArea.top = TILECY * 15.f;
	m_tArcherRecognizeArea.right = TILECX * 18.f;
	m_tArcherRecognizeArea.bottom = TILECY * 16.f;

	m_IsArcherCreated = false;

	m_bBegin = true;
}

void CStage::Update()
{
	CObjMgr::Get_Instance()->Update();
	CInventory::Get_Instance()->Update();
}

void CStage::Late_Update()
{
	CObjMgr::Get_Instance()->Late_Update();
}

void CStage::Render(HDC _DC)
{
	if (m_bBegin) {
		CSoundMgr::Get_Instance()->PlaySound(L"DUNGEON_BGM.mp3", CSoundMgr::BGM);
	}

	CTileMgr::Get_Instance()->Render(_DC);
	CObjMgr::Get_Instance()->Render(_DC);

	CInventory::Get_Instance()->Render(_DC);

	if (m_IsBossCreated == false) {
		float fX = CObjMgr::Get_Instance()->Get_Player()->Get_INFO().fX;
		float fY = CObjMgr::Get_Instance()->Get_Player()->Get_INFO().fY;
		POINT pt = { fX, fY };

		if (PtInRect(&m_tBossRecognizeArea, pt) && m_IsBossCreated == false) {
			CObj* pObj = CAbstractFactory<CMiddleBoss>::Create(17.5f * TILECX, 25.5f*TILECY, L"");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MIDDLEBOSS, pObj);
			
			m_IsBossCreated = true;
		}
	}

	if (m_bBegin == true && m_IsBossCreated && CCountMgr::Get_Instance()->GetCount() == 0) {
		CSoundMgr::Get_Instance()->StopSound(CSoundMgr::BGM);
		CSoundMgr::Get_Instance()->PlaySound(L"WIN.mp3", CSoundMgr::BGM);
		m_bBegin = false;
	}
	
	if (m_IsArcherCreated == false) {
		float fX = CObjMgr::Get_Instance()->Get_Player()->Get_INFO().fX;
		float fY = CObjMgr::Get_Instance()->Get_Player()->Get_INFO().fY;
		POINT pt = { fX, fY };

		if (PtInRect(&m_tArcherRecognizeArea, pt) && m_IsArcherCreated == false) {
			
			CObj* pObj = CAbstractFactory<CCard>::Create(14.f * TILECX, 12.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			
			pObj = CAbstractFactory<CCard>::Create(17.f * TILECX, 12.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(20.f * TILECX, 12.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(20.f * TILECX, 14.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(20.f * TILECX, 17.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(17.f * TILECX, 17.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(14.f * TILECX, 17.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			pObj = CAbstractFactory<CCard>::Create(14.f * TILECX, 14.f*TILECY, L"SUMMON_CARD_ARCHER");
			CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);
			
			m_IsArcherCreated = true;
		}

	}


}

void CStage::Release()
{
}
