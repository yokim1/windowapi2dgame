#include "stdafx.h"
#include "Shield.h"
#include "ObjMgr.h"
#include "ScrollMgr.h"
#include "BmpMgr.h"
#include "CollisionMgr.h"
#include "SoundMgr.h"

CShield::CShield()
{
}


CShield::~CShield()
{
	Release();
}

void CShield::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/GAIA_ARMOR.bmp", L"GAIA_ARMOR");
	m_tInfo.iCX = 50;
	m_tInfo.iCY = 50;

	m_fSpeed = 1.f;
	
	m_fDis = 80.f;

	m_bHit = false;

	m_iStartTime = (int)GetTickCount() / 1000;
	CSoundMgr::Get_Instance()->PlaySound(L"GAIA_ARMOR_START.mp3", CSoundMgr::EFFECT);
}

int CShield::Update()
{
	if ((((int)GetTickCount() / 1000) - m_iStartTime) >= m_iDuration) {
		Set_Dead();
	}

	if (m_bHit) {
		CSoundMgr::Get_Instance()->PlaySound(L"HIT_SOUND_NORMAL_1.mp3", CSoundMgr::EFFECT);
		m_bHit = false;
	}

	if (m_bDead)
		return OBJ_DEAD;

	//계속 +=을 해서
	//Set_State()에서 설정 해놓은 범위 안에 들기 위해,
	//%360을 해준다.
	//근데 -로는 가지 않는다.
	m_fAngle += m_fSpeed;
	m_fAngle = m_fAngle % 360;

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();
	
	
	if (m_tInfo.fY < m_pTarget->Get_INFO().fY) {
		//m_fAngle *= -1;
		float a = m_pTarget->Get_INFO().fY;
		float b = a;
	}
	else if (m_pTarget->Get_INFO().fY < m_tInfo.fY) {
		float a = m_pTarget->Get_INFO().fY;
		float b = a;
	}

	m_tInfo.fX = m_pTarget->Get_INFO().fX + cosf(m_fAngle * PI / 180.f) * m_fDis;
	m_tInfo.fY = m_pTarget->Get_INFO().fY - sinf(m_fAngle * PI / 180.f) * m_fDis;

	Update_Rect();
	Set_State();

	return OBJ_NOEVENT;
}

void CShield::Late_Update()
{

}

void CShield::Render(HDC _DC)
{
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"GAIA_ARMOR");

	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_tFrame.iStartX * 100, m_tFrame.iStateY * 105
		, 100, 105
		, RGB(255, 0, 255));
}

void CShield::Release()
{
}

void CShield::Set_State() {
	if (0 <= m_fAngle && m_fAngle <= 15) {
		m_tFrame.iStartX = 3;
	}
	else if (15 <= m_fAngle && m_fAngle <= 45) {
		m_tFrame.iStartX = 4;
	}
	else if (45 <= m_fAngle && m_fAngle <= 75) {
		m_tFrame.iStartX = 5;
	}
	else if (75 <= m_fAngle && m_fAngle <= 105) {
		m_tFrame.iStartX = 6;
	}
	else if (105 <= m_fAngle && m_fAngle <= 135) {
		m_tFrame.iStartX = 7;
	}
	else if (135 <= m_fAngle && m_fAngle <= 165) {
		m_tFrame.iStartX = 8;
	}
	else if (165 <= m_fAngle && m_fAngle <= 180) {
		m_tFrame.iStartX = 9;
	}
	else if (180 <= (m_fAngle) && (m_fAngle) <= 195) {
			m_tFrame.iStartX = 10;
		}
		else if (195 <= (m_fAngle) && (m_fAngle) <= 225) {
			m_tFrame.iStartX = 11;
		}
		else if (225 <= (m_fAngle) && (m_fAngle) <= 255) {
			m_tFrame.iStartX = 0;
		}
		else if (255 <= (m_fAngle) && (m_fAngle) <= 285) {
			m_tFrame.iStartX = 1;
		}
		else if (285 <= m_fAngle && m_fAngle <= 315) {
			m_tFrame.iStartX = 2;
		}
		else if (315 <= m_fAngle && m_fAngle <= 345) {
			m_tFrame.iStartX = 3;
		}
		else if (345 <= m_fAngle && m_fAngle <= 360) {
			m_tFrame.iStartX = 4;
		}
}

void CShield::Update_HitBox() {
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	m_tHitBox.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1)) + iScrollY;
	m_tHitBox.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 2)) + iScrollX;
	m_tHitBox.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1)) + iScrollY;
}