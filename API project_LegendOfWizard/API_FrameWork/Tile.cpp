#include "stdafx.h"
#include "Tile.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "KeyMgr.h"


CTile::CTile()
{
}


CTile::~CTile()
{
	Release();
}

void CTile::Initialize()
{
	//Ÿ�� ũ��  = 105 x 105

	m_tInfo.iCX = TILECX;
	m_tInfo.iCY = TILECY;

	turnOnGrid = false;
}

int CTile::Update()
{
	

	return 0;
}

void CTile::Late_Update()
{
}

void CTile::Render(HDC _DC)
{
	Update_Rect();

	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"WOL_TILE_HOMETOWN");

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, m_iDrawIDX * 145, m_iDrawIDY * 145
		, 145, 145
		, RGB(255, 0, 255));
}

void CTile::Release()
{
}
