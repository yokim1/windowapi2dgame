#include "stdafx.h"
#include "Obj.h"
#include "ScrollMgr.h"

CObj::CObj()
	: m_fSpeed(0.f), m_bDead(false),m_bHit(false), m_fAngle(0.f), m_pTarget(nullptr)
	, m_fDis(0.f), m_pFrameKey(nullptr), m_iDrawID(0), m_iAngle(0)
{
	ZeroMemory(&m_tInfo, sizeof(m_tInfo));
	ZeroMemory(&m_tRect, sizeof(m_tRect));
	ZeroMemory(&m_tFrame, sizeof(m_tFrame));
}


CObj::~CObj()
{
}

void CObj::RenderHitBox(HDC _DC) {
	Rectangle(_DC, m_tHitBox.left, m_tHitBox.top, m_tHitBox.right, m_tHitBox.bottom);
}

void CObj::Update_Rect()
{
	Update_HitBox();
	m_tRect.left = (LONG)(m_tInfo.fX - (m_tInfo.iCX >> 1));
	m_tRect.top = (LONG)(m_tInfo.fY - (m_tInfo.iCY >> 1));
	m_tRect.right = (LONG)(m_tInfo.fX + (m_tInfo.iCX >> 1));
	m_tRect.bottom = (LONG)(m_tInfo.fY + (m_tInfo.iCY >> 1));
}

void CObj::Update_Frame()
{
	/*if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			if (m_eCurState == BASICATTACK || m_eCurState == DASH) {
				m_eCurState = IDLE;
			}

			
			m_tFrame.iStartX = 0;
		}
	}*/
}

void CObj::Update_HitBox() {

}