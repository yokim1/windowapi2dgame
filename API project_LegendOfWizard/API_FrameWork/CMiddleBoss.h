#pragma once
#include "Obj.h"
class CMiddleBoss : public CObj
{
public:
	enum STATE { IDLE, WALK, ATTACK, ATTACK2, HIT, DEAD, END };

public:
	CMiddleBoss();
	virtual ~CMiddleBoss();

public:
	virtual void Initialize() override;
	virtual int Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	void Update_Frame();
	void Set_State();

	void Update_Range();
	void Key_State();

	void Update_HitBox() override;

private:
	template <typename T>
	CObj* Create_Bullet(float fX, float fY)
	{
		CObj* pObj = CAbstractFactory<T>::Create(fX, fY, m_fAngle);

		return pObj;
	}

private:
	//float	m_fRadius;
	RECT	m_tRangeRect;
	RECT	m_tAttackRangeRect;

	STATE	m_eCurState;
	STATE	m_ePreState;

	bool m_bSummonSword = true;
	bool m_bSummonArcher = true;
	bool m_bSummonWizard = true;
};

