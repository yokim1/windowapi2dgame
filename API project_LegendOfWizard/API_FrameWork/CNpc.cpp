#include "stdafx.h"
#include "CNpc.h"
#include "BmpMgr.h"
#include "ScrollMgr.h"
#include "ObjMgr.h"

CNpc::CNpc()
{
}


CNpc::~CNpc()
{
}


void CNpc::Initialize() {
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ITEMSHOP_NPC.bmp", L"ITEMSHOP_NPC");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/SHOP_TABLE.bmp", L"SHOP_TABLE");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/POTION.bmp", L"POTION");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/GAIA_ARMOR_CARD.bmp", L"GAIA_ARMOR_CARD");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/TELEPORT.bmp", L"TELEPORT");

	m_eRenderID = RENDERID::OBJECT;

	m_tInfo.iCX = 64;
	m_tInfo.iCY = 98;
 }

int CNpc::Update() {

	Update_Rect();
	return OBJ_NOEVENT;
}
 
 void CNpc::Late_Update() {

 }
 
 void CNpc::Render(HDC _DC) {

	 int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	 int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	 HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"ITEMSHOP_NPC");
	 GdiTransparentBlt(_DC
		 , m_tRect.left + iScrollX, m_tRect.top + iScrollY
		 , m_tInfo.iCX, m_tInfo.iCY
		 , hMemDC
		 , 0, 0
		 , 128, 197
		 , RGB(255, 0, 255));

	 hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"SHOP_TABLE");
	 GdiTransparentBlt(_DC
		 , m_tRect.left + 70 + iScrollX, m_tRect.top + iScrollY
		 , 300, 100
		 , hMemDC
		 , 0, 0
		 , 606, 234
		 , RGB(255, 0, 255));

	 hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"POTION");
	 GdiTransparentBlt(_DC
		 , m_tRect.left + 130 + iScrollX, m_tRect.top - 20 + iScrollY
		 , 50, 70
		 , hMemDC
		 , 0, 0
		 , 106, 155
		 , RGB(255, 0, 255));

	 //hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"GAIA_ARMOR_CARD");
	 //GdiTransparentBlt(_DC
		// , m_tRect.left + 220 + iScrollX, m_tRect.top - 25 + iScrollY
		// , 50, 80
		// , hMemDC
		// , 0, 0
		// , 106, 172
		// , RGB(255, 0, 255));


	 hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"TELEPORT");
	 GdiTransparentBlt(_DC
		 , 32.f * TILECX + iScrollX, 13.5f*TILECY + iScrollY
		 , 200, 200
		 , hMemDC
		 , 0, 0
		 , 311, 298
		 , RGB(255, 0, 255));
 }

void CNpc::Release() {

 }