#include "stdafx.h"
#include "TileMgr.h"
#include "Tile.h"
#include "ScrollMgr.h"
#include "KeyMgr.h"

CTileMgr* CTileMgr::m_pInstance = nullptr;

CTileMgr::CTileMgr()
	: m_iSize(TILEX * TILEY){
	m_vecTile.reserve(m_iSize);
}


CTileMgr::~CTileMgr()
{
	Release();
}

void CTileMgr::Initialize()
{
	for (int i = 0; i < TILEY; ++i)
	{
		for (int j = 0; j < TILEX; ++j)
		{
			float fX = (float)(j * TILECX + (TILECX >> 1));
			float fY = (float)(i * TILECY + (TILECY >> 1));

			CObj* pObj = CAbstractFactory<CTile>::Create(fX, fY);
			m_vecTile.emplace_back(pObj);
		}
	}

	//MAP tile 
	// tile x size = 105
	// tile y size = 105

	//WOL_TILE_DUNGEON 
	// tile x size = 145 
	// tile y size = 145

	//WOL_TILE_HOMETOWN
	// tree x size = 360
	// tree y size = 500

	// tree x size = 360
	// tree y size = 340
}

void CTileMgr::Render(HDC _DC)
{
	ShowCursor(true);
	
	int iScrollX = abs((int)CScrollMgr::Get_Instance()->Get_ScrollX());
	int iScrollY = abs((int)CScrollMgr::Get_Instance()->Get_ScrollY());

	int	iCullX = iScrollX / TILECX;
	int iCullY = iScrollY / TILECY;

	int iCullEndX = iCullX + (WINCX / TILECX) + 2;
	int iCullEndY = iCullY + (WINCY / TILECY) + 2;

	for (int i = iCullY; i < iCullEndY; ++i)
	{
		for (int j = iCullX; j < iCullEndX; ++j)
		{
			size_t iIdx = i * TILEX + j;

			if(0 > iIdx || m_vecTile.size() <= iIdx)
				continue;

			m_vecTile[iIdx]->Render(_DC);
		}
	}

	if (CKeyMgr::Get_Instance()->Key_Down('G')) {
		turnOnGrid = !turnOnGrid;
		turnOnWall = !turnOnWall;
	}

	if (turnOnGrid) {
		// lines for 세로
		for (int x = 1; x < 50; ++x) {
			MoveToEx(_DC, (x * TILECX) - iScrollX, 0 - iScrollY, nullptr);
			LineTo(_DC, (x * TILECX) - iScrollX, 10000 - iScrollY);
		}

		// lines for 가로
		for (int x = 1; x < 500; ++x) {
			MoveToEx(_DC, 0 - iScrollX, (x * TILECY) - iScrollY, nullptr);
			LineTo(_DC, 10000 - iScrollX, (x * TILECY) - iScrollY);
		}
	}
	
}

void CTileMgr::Release()
{
	for_each(m_vecTile.begin(), m_vecTile.end(), Safe_Delete<CObj*>);
	m_vecTile.clear();
}

void CTileMgr::Tile_Picking(int _iDrawID)
{
	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	POINT	pt = {};
	GetCursorPos(&pt);
	
	ScreenToClient(g_hWnd, &pt);

	pt.x -= iScrollX;
	pt.y -= iScrollY;

	int	x = (pt.x / TILECX);
	int	y = (pt.y / TILECY);

	int		iIdx = (TILEX * y + x);
	int		ildy = (TILEY * y + x);

	if (0 > iIdx || m_vecTile.size() <= (size_t)iIdx)
		return;

	if (0 > ildy || m_vecTile.size() <= (size_t)ildy)
		return;

	m_vecTile[iIdx]->Set_DrawID(_iDrawID);
	//m_vecTile[ildy]->Set_DrawID(_iDrawID);
}

void CTileMgr::Tile_Picking(int _iDrawIDX, int _iDrawIDY)
{
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();
	
	int idX;
	int idY;

	for (int y = 0; y < 7; ++y) {
		for (int x = 0; x < 10; ++x) {
			if ((( (x * 30) + 200 - iScrollX) <= _iDrawIDX && _iDrawIDX <= ((x * 30) + 230 - iScrollX)) &&
				(( (y * 30) + 150 - iScrollY) <= _iDrawIDY && _iDrawIDY <= ((y * 30) + 180 - iScrollY))) {
				idX = x;
				idY = y;
			}
		}
	}

	//+++++++++++++++++++++++++++++++++++++++
	//I need to figure out WALL or not here//
	//+++++++++++++++++++++++++++++++++++++++

	POINT	pt = {};
	GetCursorPos(&pt);

	ScreenToClient(g_hWnd, &pt);

	pt.x -= iScrollX;
	pt.y -= iScrollY;

	int	x = (pt.x / (TILECX));
	int	y = (pt.y / (TILECY));

	int		iIdx = (TILEX * y + x);

	if (0 > iIdx || m_vecTile.size() <= (size_t)iIdx)
		return;


	if (_iDrawIDX == 0 && _iDrawIDY == 0) {
		m_vecTile[iIdx]->Set_DrawIDPt(0, 0);
		return;
	}

	m_vecTile[iIdx]->Set_DrawIDPt(idX, idY);
}

void CTileMgr::Save_Tile()
{
	HANDLE hFile = CreateFile(L"../Data/Tile.dat", GENERIC_WRITE
		, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"저장 실패", L"실패", MB_OK);
		return;
	}

	DWORD	dwByte = 0;
	for (auto& pTile : m_vecTile)
	{
		WriteFile(hFile, &pTile->Get_INFO(), sizeof(INFO), &dwByte, NULL);

		WriteFile(hFile, &pTile->Get_DrawIDX(), sizeof(int), &dwByte, NULL);
		WriteFile(hFile, &pTile->Get_DrawIDY(), sizeof(int), &dwByte, NULL);
	}

	CloseHandle(hFile);
	MessageBox(g_hWnd, L"저장 성공", L"성공", MB_OK);
}

void CTileMgr::Load_Tile()
{
	HANDLE hFile = CreateFile(L"../Data/Tile.dat", GENERIC_READ
		, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE == hFile)
	{
		MessageBox(g_hWnd, L"불러오기 실패", L"실패", MB_OK);
		return;
	}

	Release();
	DWORD	dwByte = 0;
	INFO	tInfo = {};
	int		iDrawID = 0;

	int iDrawIDX = 0;
	int iDrawIDY = 0;

	while (true)
	{
		ReadFile(hFile, &tInfo, sizeof(INFO), &dwByte, NULL);

		ReadFile(hFile, &iDrawIDX, sizeof(int), &dwByte, NULL);
		ReadFile(hFile, &iDrawIDY, sizeof(int), &dwByte, NULL);
		
		if (0 == dwByte)
			break;

		CObj* pObj = CAbstractFactory<CTile>::Create(tInfo.fX, tInfo.fY);
		pObj->Set_DrawIDPt(iDrawIDX, iDrawIDY);
		m_vecTile.emplace_back(pObj);
	}

	CloseHandle(hFile);
	MessageBox(g_hWnd, L"불러오기 성공", L"성공", MB_OK);
}
