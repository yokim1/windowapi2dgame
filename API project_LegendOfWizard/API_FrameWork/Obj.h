#pragma once

#ifndef __OBJ_H__
#define __OBJ_H__

class CObj
{
public:
	CObj();
	virtual ~CObj();

public:
	virtual void Initialize() = 0;
	virtual int Update() = 0;
	virtual void Late_Update() = 0;
	virtual void Render(HDC _DC) = 0;
	virtual void RenderHitBox(HDC _DC);
	virtual void Release() = 0;

public:
	void Set_Pos(float _x, float _y) { m_tInfo.fX = _x; m_tInfo.fY = _y; }
	void Set_Dead() { m_bDead = true; }
	
	void Set_Hit() { m_bHit = true; }
	
	void Set_Angle(float _fAngle) { m_fAngle = _fAngle; }
	void Set_Target(CObj* _pTarget) { m_pTarget = _pTarget; }
	
	void Set_PosX(float _x) { m_tInfo.fX += _x; }
	void Set_PosY(float _y) { m_tInfo.fY += _y; }
	
	void Set_FrameKey(const TCHAR* _pFrameKey) { m_pFrameKey = _pFrameKey; }
	
	void Set_DrawID(int _iDrawID) { m_iDrawID = _iDrawID; }
	void Set_DrawIDPt(int _iDrawIDX, int _iDrawIDY) { m_iDrawIDX = _iDrawIDX; m_iDrawIDY = _iDrawIDY;};
	void Set_Wall(bool _isWall) { m_bIsWall = _isWall; }

	void Set_Coin(int _Coin) { m_iCoin += _Coin; }

	void Set_Reverse(bool _isReverse) { isReverse = _isReverse; }

	void Set_Speed(int _speed) { m_fSpeed = (float)_speed; }

	void IncreaseMp() { if(m_tInfo.iMP <= 95) m_tInfo.iMP++; }

public:
	const RECT& Get_HitBox() const { return m_tHitBox; }

	const RECT& Get_Rect() const { return m_tRect; }
	const INFO& Get_INFO() const { return m_tInfo; }
	
	const int& Get_DrawID() const { return m_iDrawID; }
	
	const int& Get_DrawIDX() const { return m_iDrawIDX; }
	const int& Get_DrawIDY() const { return m_iDrawIDY; }
	const bool& Get_Wall() const { return m_bIsWall; }
	const int Get_Coin() const { return m_iCoin; }

	const RENDERID::ID& Get_RenderID() const { return m_eRenderID; }

protected:
	virtual void Update_Rect();
	virtual void Update_Frame();
	virtual void Update_HitBox();

protected:
	INFO	m_tInfo;
	RECT	m_tRect;

	RECT	m_tHitBox;

	FRAME	m_tFrame;

	int		m_iDrawID;
	
	int		m_iDrawIDX;
	int		m_iDrawIDY;

	float	m_fSpeed;
	int		m_fAngle; // need to fix this to float
	float	m_fDis;

	bool	m_bAttack;
	bool	m_bDead;
	bool	m_bHit;

	CObj*	m_pTarget;

	const TCHAR*	m_pFrameKey;

	int		m_iAngle;

	int		m_iCoin;

	bool m_bIsWall;

	bool isReverse;

	RENDERID::ID m_eRenderID;
};


#endif // !__OBJ_H__
