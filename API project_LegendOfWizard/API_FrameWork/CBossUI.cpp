#include "stdafx.h"
#include "CBossUI.h"
#include "BmpMgr.h"


CBossUI::CBossUI()
{
}


CBossUI::~CBossUI()
{
}

void CBossUI::Initialize() {

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/BOSS_NAMEBAR.bmp", L"BOSS_NAMEBAR");

	m_eRenderID = RENDERID::UI;

	m_tInfo.fX = 100.f;
	m_tInfo.fY = 600.f;

	m_tInfo.iCX = 280;//64;
	m_tInfo.iCY = 88;//16;
}

int CBossUI::Update() {
	if (m_bDead) {
		return OBJ_DEAD;
	}

	Update_Rect();

	return OBJ_NOEVENT;
}

void CBossUI::Late_Update() {

}

void CBossUI::Render(HDC _DC) {

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"BOSS_NAMEBAR");

	GdiTransparentBlt(_DC
		, 50, 500
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, 0, 0
		, 640, 160
		, RGB(255, 0, 255));
}

void CBossUI::Release() {

}
