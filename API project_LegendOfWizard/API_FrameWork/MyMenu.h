#pragma once

#ifndef __MYMENU_H__
#define __MYMENU_H__


#include "Scene.h"
class CMyMenu : public CScene
{
public:
	CMyMenu();
	virtual ~CMyMenu();

public:
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Late_Update() override;
	virtual void Render(HDC _DC) override;
	virtual void Release() override;

private:
	enum MENU_CHOICE{MAIN, OPTION, QUIT, END};
	int m_eChoice;

	/*const RECT m_tMenuRectMAIN = {860, 545, 1055, 575};
	const RECT m_tMenuRectOPTION = { 900, 780, 1010, 810 };
	const RECT m_tMenuRectQUIT = { 920, 930, 1000, 970 };*/

	const RECT m_tMenuRectMAIN = { 380, 290, 420, 330 };
	const RECT m_tMenuRectOPTION = { 380, 400, 420, 460 };
	const RECT m_tMenuRectQUIT = { 380, 510, 420, 540 };
};


#endif // !__MYMENU_H__
