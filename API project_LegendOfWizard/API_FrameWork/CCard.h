#pragma once
#include "Obj.h"
class CCard :
	public CObj
{
	enum STATE {START, DESTROY = 31, END};
public:
	CCard();
	virtual ~CCard();

public:
	virtual void Initialize() ;
	virtual int Update();
	virtual void Late_Update();
	virtual void Render(HDC _DC) ;
	virtual void Release();

	void Update_Frame();

private:
	STATE m_eCurState;
	STATE m_ePreState;

	bool Summoned;
};

