#include "stdafx.h"
#include "CCard.h"

#include "ScrollMgr.h"
#include "ObjMgr.h"
#include "BmpMgr.h"

#include "Monster.h"
#include "CArcher.h"
#include "CWizard.h"
#include "CWizardBall.h"
#include "SoundMgr.h"

CCard::CCard()
{
}


CCard::~CCard()
{
}



void CCard::Initialize() {
	m_eCurState = END;
	m_ePreState = END;

	m_tFrame.iStartX = 0;
	m_tFrame.iEndX = 31;
	m_tFrame.iStateY = START;
	m_tFrame.dwSpeed = 50;
	m_tFrame.dwTime = GetTickCount();

	Summoned = false;

	
}

int CCard::Update() {
	if (m_bDead)
		return OBJ_DEAD;

	Update_Frame();

	if (m_tFrame.iStartX == 1) {
		CSoundMgr::Get_Instance()->PlaySound(L"CARD_SUMMON.mp3", CSoundMgr::EFFECT);
	}

	if (m_tFrame.iStartX == 27 && Summoned == false)
	{
		CObj* pObj = nullptr;
		
		if(lstrcmp(L"SUMMON_CARD_SWORDMAN", m_pFrameKey) == 0)
			 pObj  = CAbstractFactory<CMonster>::Create(m_tInfo.fX + 50, m_tInfo.fY + 50);
		
		else if(lstrcmp(L"SUMMON_CARD_ARCHER", m_pFrameKey) == 0)
			pObj = CAbstractFactory<CArcher>::Create(m_tInfo.fX + 50, m_tInfo.fY + 50);

		else if(lstrcmp(L"SUMMON_CARD_WIZARD", m_pFrameKey) == 0)
			pObj = CAbstractFactory<CWizard>::Create(m_tInfo.fX + 50, m_tInfo.fY + 50);

		else if (lstrcmp(L"SUMMON_CARD_WIZARDBALL", m_pFrameKey) == 0)
			pObj = CAbstractFactory<CWizard>::Create(m_tInfo.fX + 50, m_tInfo.fY + 50);

		CObjMgr::Get_Instance()->Add_Object(OBJID::MONSTER, pObj);

		Summoned = !Summoned;
	}

	return OBJ_NOEVENT;
}

void CCard::Late_Update() {

	//if (m_eCurState != m_ePreState) {
	//	m_tFrame.iStartX = 0;
	//	m_tFrame.iEndX = 0;
	//	m_tFrame.iStateY = START;
	//	m_tFrame.dwSpeed = 100;
	//	m_tFrame.dwTime = GetTickCount();

	//	m_ePreState = m_eCurState;
	//}
}

void CCard::Render(HDC _DC) {
	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	Update_Rect();

	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(m_pFrameKey);

	GdiTransparentBlt(_DC
		, m_tRect.left + iScrollX, m_tRect.top + iScrollY
		, 100, 100
		, hMemDC
		, m_tFrame.iStartX * 150, m_tFrame.iStateY * 230
		, 150, 230
		, RGB(255, 0, 255));

	//Rectangle(_DC, m_tRect.left + iScrollX, m_tRect.top + iScrollY, m_tRect.right + 100 + iScrollX, m_tRect.bottom + 100 + iScrollY);
}

void CCard::Release() {

}

void CCard::Update_Frame()
{
	if (m_tFrame.dwTime + m_tFrame.dwSpeed < GetTickCount())
	{
		++m_tFrame.iStartX;
		m_tFrame.dwTime = GetTickCount();

		if (m_tFrame.iStartX >= m_tFrame.iEndX)
		{
			Set_Dead();
			m_tFrame.iStartX = 0;
		}
	}
}