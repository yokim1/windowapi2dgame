#include "stdafx.h"
#include "CPlayerUI.h"
#include "BmpMgr.h"
#include "ObjMgr.h"


CPlayerUI::CPlayerUI()
{
}


CPlayerUI::~CPlayerUI()
{
}

void CPlayerUI::Initialize() {

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_SKILLBAR.bmp", L"UI_SKILLBAR");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_PLAYERBAR.bmp", L"UI_PLAYERBAR");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_MONEY.bmp", L"UI_MONEY");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_HPBAR.bmp", L"UI_HPBAR");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/UI_MANABAR.bmp", L"UI_MANABAR");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/NORMAL_ATTACK_ICON.bmp", L"NORMAL_ATTACK_ICON");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/FIRE_DRAGON_SKILLBAR.bmp", L"FIRE_DRAGON_SKILLBAR");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/GAIA_ARMOR_SKILLBAR.bmp", L"GAIA_ARMOR_SKILLBAR");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/ICE_KRYSTAL_SKILLBAR.bmp", L"ICE_KRYSTAL_SKILLBAR");


	m_eRenderID = RENDERID::UI;

	m_tInfo.fX = 100.f;
	m_tInfo.fY = 600.f;

	m_tInfo.iCX = 320;//64;
	m_tInfo.iCY = 80;//16;

	m_iCoin = 0;

	qItem.left = 170;
	qItem.top = 533;
	
	eItem.left = 208;
	eItem.top = 533;

	rItem.left = 246;
	rItem.top = 533;
}

int CPlayerUI::Update() {
	if (m_bDead) {
		return OBJ_DEAD;
	}

	m_pTarget = CObjMgr::Get_Instance()->Get_Player();
	
	m_iHp = m_pTarget->Get_INFO().iHP;

	m_iMp = m_pTarget->Get_INFO().iMP;

	m_iCoin = m_pTarget->Get_Coin();

	Update_Rect();

	return OBJ_NOEVENT;
}

void CPlayerUI::Late_Update() {

}

void CPlayerUI::Render(HDC _DC) {
	
	HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_SKILLBAR");
		
	GdiTransparentBlt(_DC
		, 50, 500
		, m_tInfo.iCX, m_tInfo.iCY
		, hMemDC
		, 0, 0
		, 640, 160
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_PLAYERBAR");

	GdiTransparentBlt(_DC
		, 50, 50
		, 164, 40
		, hMemDC
		, 0, 0
		, 328, 80
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_MONEY");

	GdiTransparentBlt(_DC
		, 400, 535
		, 15, 15
		, hMemDC
		, 0, 0
		, 31, 31
		, RGB(255, 0, 255));

	TCHAR lpOut[1000];
	wsprintf(lpOut, TEXT("%d"), m_iCoin);
	TextOut(_DC, 425, 535, lpOut, lstrlen(lpOut));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_HPBAR");
	GdiTransparentBlt(_DC
		, 86, 55 //hp bar xy 위치
		, m_iHp, 17 //hp 길이 높이
		, hMemDC
		, 0, 0
		, 244, 32
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"UI_MANABAR");
	GdiTransparentBlt(_DC
		, 88, 75 //mp bar xy 위치
		, m_iMp, 8 //mp 길이 높이
		, hMemDC
		, 0, 0
		, 192, 16
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"NORMAL_ATTACK_ICON");
	GdiTransparentBlt(_DC
		, 130, 530
		, 28, 28
		, hMemDC
		, 0, 0
		, 52, 52
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"FIRE_DRAGON_SKILLBAR");
	GdiTransparentBlt(_DC
		, qItem.left, qItem.top
		, 28, 28
		, hMemDC
		, 0, 0
		, 52, 52
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"GAIA_ARMOR_SKILLBAR");
	GdiTransparentBlt(_DC
		, eItem.left, eItem.top
		, 28, 28
		, hMemDC
		, 0, 0
		, 52, 52
		, RGB(255, 0, 255));

	hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"ICE_KRYSTAL_SKILLBAR");
	GdiTransparentBlt(_DC
		, rItem.left, rItem.top
		, 28, 28
		, hMemDC
		, 0, 0
		, 52, 52
		, RGB(255, 0, 255));
}

void CPlayerUI::Release() {

}
