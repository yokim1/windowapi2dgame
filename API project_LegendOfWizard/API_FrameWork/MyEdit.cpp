#include "stdafx.h"
#include "MyEdit.h"
#include "BmpMgr.h"
#include "TileMgr.h"
#include "..\Editor\KeyMgr.h"
#include "ScrollMgr.h"


CMyEdit::CMyEdit()
{
}


CMyEdit::~CMyEdit()
{
	Release();
}

void CMyEdit::Initialize()
{
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/Tile.bmp", L"Tile");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MAPTILE_1.bmp", L"MAPTILE_1");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WOL_TILE_HOMETOWN.bmp", L"WOL_TILE_HOMETOWN");

	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MAPTILE_2.bmp", L"MAPTILE_2");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/MAPTILE_3.bmp", L"MAPTILE_3");
	CBmpMgr::Get_Instance()->Insert_Bmp(L"../Image/WOL_TEXTURE/WOL_TILE_DUNGEON.bmp", L"WOL_TILE_DUNGEON");

	CTileMgr::Get_Instance()->Initialize();

	m_bIsMapTile = false;
}

void CMyEdit::Update()
{
	int iScrollX = CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = CScrollMgr::Get_Instance()->Get_ScrollY();

	if (CKeyMgr::Get_Instance()->Key_Down('M')) {
		m_bIsMapTile = !m_bIsMapTile;
	}
	
	if(m_bIsMapTile == false) {
		if (CKeyMgr::Get_Instance()->Key_Pressing(VK_LEFT))
			CScrollMgr::Get_Instance()->Set_ScrollX(5.f);
		if (CKeyMgr::Get_Instance()->Key_Pressing(VK_RIGHT))
			CScrollMgr::Get_Instance()->Set_ScrollX(-5.f);

		if (CKeyMgr::Get_Instance()->Key_Pressing(VK_UP))
			CScrollMgr::Get_Instance()->Set_ScrollY(5.f);
		if (CKeyMgr::Get_Instance()->Key_Pressing(VK_DOWN))
			CScrollMgr::Get_Instance()->Set_ScrollY(-5.f);

		if (CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON))
		
			if ((200 - iScrollX <= mapPos.x && mapPos.x <= 490 - iScrollX) &&
				(150 - iScrollY <= mapPos.y && mapPos.y <= 340 - iScrollY)) {
				CTileMgr::Get_Instance()->Tile_Picking(mapPos.x, mapPos.y);
			}
		if (CKeyMgr::Get_Instance()->Key_Down(VK_RBUTTON))
			{ 
			CTileMgr::Get_Instance()->Tile_Picking(0, 0);
			}
	}
	else {
		if (CKeyMgr::Get_Instance()->Key_Down(VK_LBUTTON)) {
			mapPos.x = pt.x;
			mapPos.y = pt.y;
		}
	}

	if (CKeyMgr::Get_Instance()->Key_Down('S'))
		CTileMgr::Get_Instance()->Save_Tile();
	if (CKeyMgr::Get_Instance()->Key_Down('L'))
		CTileMgr::Get_Instance()->Load_Tile();
}

void CMyEdit::Late_Update()
{
}

void CMyEdit::Render(HDC _DC)
{
	int iScrollX = (int)CScrollMgr::Get_Instance()->Get_ScrollX();
	int iScrollY = (int)CScrollMgr::Get_Instance()->Get_ScrollY();

	GetCursorPos(&pt);

	ScreenToClient(g_hWnd, &pt);

	pt.x -= iScrollX;
	pt.y -= iScrollY;

	CTileMgr::Get_Instance()->Render(_DC);
	
	//M을 누르면 maptile이 보여진다.
	if (m_bIsMapTile) {
		HDC hMemDC = CBmpMgr::Get_Instance()->Find_Bmp(L"WOL_TILE_HOMETOWN");

		GdiTransparentBlt(_DC
			, WINCX>>2, WINCY>>2
			, 384, 216
			, hMemDC
			, 0, 0
			, 1920, 1080
			, RGB(255, 0, 255));
	}

	// 마우스의 위치 표시
	POINT pos = {NULL, NULL};

	for (int x = 0; x < TILEX; ++x) {
		if (x <= pt.x && pt.x <= (TILECX * (x + 1))) {
			pos.x = x; break;
		}
	}

	for (int y = 0; y < TILEY; ++y) {
		if (y <= pt.y && pt.y <= (TILECY * (y + 1))) {
			pos.y = y; break;
		}
	}

	TCHAR lpOut[1000];
	wsprintf(lpOut, TEXT("X: %d"), (int)pos.x);
	TextOut(_DC, 100, 100, lpOut, lstrlen(lpOut));

	wsprintf(lpOut, TEXT("Y: %d"), (int)pos.y);
	TextOut(_DC, 150, 100, lpOut, lstrlen(lpOut));

	//maptile의 위치 표시
	wsprintf(lpOut, TEXT("tile X: %d"), (int)mapPos.x);
	TextOut(_DC, 100, 200, lpOut, lstrlen(lpOut));

	wsprintf(lpOut, TEXT("tile Y: %d"), (int)mapPos.y);
	TextOut(_DC, 100, 300, lpOut, lstrlen(lpOut));
}

void CMyEdit::Release()
{
}
