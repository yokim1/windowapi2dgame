#pragma once

#ifndef __FUNCTOR_H__
#define __FUNCTOR_H__

class CStrCmp
{
public:
	CStrCmp(const TCHAR* _pString) : m_String(_pString) {}

public:
	template <typename T>
	bool operator()(T& _Dst)
	{
		return !lstrcmp(m_String, _Dst.first);
	}

private:
	const TCHAR*	m_String;
};

#endif // !__FUNCTOR_H__
