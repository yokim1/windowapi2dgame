#pragma once

#ifndef __DEFINE_H__
#define __DEFINE_H__

#define WINCX	800//1920//800
#define WINCY	600//1080//600

#define SAFE_DELETE(p) if(p) { delete p; p = nullptr; }

#define OBJ_NOEVENT 0
#define OBJ_DEAD 1

#define PI 3.141592f

#define TILECX 72//(145>>1)//64
#define TILECY 72//(145>>1)//64
#define TILEX 100
#define TILEY 100

#endif // !__DEFINE_H__
